package main;

import java.util.Calendar;
import java.util.Date;

public class Main {

	public static void main(String[] args) {
		
		//Una forma muy com�n en java
		// para sacar la fecha y hora actual es instanciar la clase Date
		Date d1 = new Date();
		
		System.out.println("Ahora es :" + d1.toString());
		//Calendar nos vale para obtener de forma c�moda la fecha y hora de un Date
		Calendar c = Calendar.getInstance();
		c.setTime(d1);
		System.out.println("D�a de hoy: "+ c.get(Calendar.DAY_OF_MONTH));
		//El primer mes es el 0 cero (enero)
		System.out.println("Mes: "+ c.get(Calendar.MONTH));
		System.out.println("A�o: " + c.get(Calendar.YEAR));
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Date masTarde = new Date();
		System.out.println("masTarde vale: " +  masTarde.toString()); 
		
		long diferencia = masTarde.getTime()-d1.getTime();
		System.out.println("Diferencia: " + diferencia);
		
	}

}
