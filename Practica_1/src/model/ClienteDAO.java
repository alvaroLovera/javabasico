package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO extends MasterDAO {

	public ClienteDAO() {
	}

	public void guardarCliente(Cliente c) {

		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_INSERCION_CLIENTE);
			ps.setString(1, c.getNombre());
			ps.setString(2, c.getApellidos());
			ps.setInt(3, c.getEdad());
			ps.setString(4, c.getCervezaFavorita());
			ps.setBoolean(5, c.isAlcoholismo());

			ps.execute();
		} catch (SQLException e) {
			System.out.println("Error Prepared Stament: " + e.getMessage());
			e.printStackTrace();
		}
		desconectar();
	}

	public ArrayList<Cliente> getClientes() {

		List<Cliente> clientesBaseDatos = new ArrayList<Cliente>();
		String sql = ConstantesSQL.SQL_SELECT_CLIENTES;
		conectar();
		Statement st;
		try {
			st = conexion.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				Cliente c = new Cliente();
				c.setId(rs.getInt("id"));
				c.setNombre(rs.getString(2));
				c.setApellidos(rs.getString(3));
				c.setEdad(rs.getInt(4));
				c.setCervezaFavorita(rs.getString(5));
				c.setAlcoholismo(rs.getBoolean(6));

				clientesBaseDatos.add(c);
			}
		} catch (SQLException e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
		desconectar();
		return (ArrayList) clientesBaseDatos;
	}
	
	
	public void borrarCliente(int id) {

		conectar();

		String sql = ConstantesSQL.SQL_DELETE_CLIENTE;
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setInt(1, id);
			ps.execute();

		} catch (SQLException e) {
			System.out.println("ERROR Prepared Statement: " + e.getMessage());
			e.printStackTrace();
		}

		desconectar();

	}
	
	public void editarCliente(Cliente clienteAeditar) {

		conectar();
		String sql = ConstantesSQL.SQL_UPDATE_CLIENTE;
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setString(1, clienteAeditar.getNombre());
			ps.setString(2, clienteAeditar.getApellidos());
			ps.setInt(3, clienteAeditar.getEdad());
			ps.setString(4, clienteAeditar.getCervezaFavorita());
			ps.setBoolean(5, clienteAeditar.isAlcoholismo());
			ps.setInt(6, clienteAeditar.getId());

			ps.execute();
		} catch (SQLException e) {
			System.out.println("ERROR Prepared Statement: " + e.getMessage());
			e.printStackTrace();
		}
		desconectar();
	}

	public Cliente buscarCliente(int idAeditar) {
		conectar();
		Cliente cliente = new Cliente();
		String sql = ConstantesSQL.SQL_OBTENER_CLIENTE_POR_ID;
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setInt(1, idAeditar);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				cliente.setId(rs.getInt("id"));
				cliente.setNombre(rs.getString("nombre"));
				cliente.setApellidos(rs.getString("apellidos"));
				cliente.setEdad(rs.getInt("edad"));
				cliente.setCervezaFavorita(rs.getString("cervezaFavorita"));
				cliente.setAlcoholismo(rs.getBoolean("alcoholico"));
			}
		} catch (SQLException e) {
			System.out.println("ERROR PREPA.STAMENT: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		desconectar();
		return cliente;
	}

}
