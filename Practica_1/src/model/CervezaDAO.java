package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PRAcroForm;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

public class CervezaDAO extends MasterDAO {

//	private static final String NOMBRE_ARCHIVO = "cervezas.dat";
//	private ArrayList<Cerveza> cervezas = new ArrayList<Cerveza>();
	private static final CervezaDAO instancia = new CervezaDAO();

	private CervezaDAO() {

	}

	public static CervezaDAO getInstancia() {
		return instancia;
	}

	public void guardarCerveza(Cerveza c) {

		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_INSERCION_CERVEZA);
			ps.setString(1, c.getMarca());
			ps.setDouble(2, c.getPrecio());
			ps.setInt(3, c.getCantidad());
			ps.setString(4, c.getIngredientes());
			ps.setString(5, c.getOpinion());
			ps.setString(6, c.getDescripcion());

			ps.execute();
		} catch (SQLException e) {
			System.out.println("Error Prepared Stament: " + e.getMessage());
			e.printStackTrace();
		}
		// cervezas.add(c);
		desconectar();
	}

	public ArrayList<Cerveza> getCervezas() {

		List<Cerveza> cervezaBaseDatos = new ArrayList<Cerveza>();
		String sql = ConstantesSQL.SQL_SELECT_CERVEZAS;
		conectar();
		Statement st;
		try {
			st = conexion.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				Cerveza cerveza = new Cerveza();
				cerveza.setId(rs.getInt("id"));
				cerveza.setMarca(rs.getString(2));
				cerveza.setPrecio(rs.getDouble(3));
				cerveza.setCantidad(rs.getInt(4));
				cerveza.setIngredientes(rs.getString(5));
				cerveza.setOpinion(rs.getString(6));
				cerveza.setDescripcion(rs.getString(7));

				cervezaBaseDatos.add(cerveza);
			}

		} catch (SQLException e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}

		desconectar();
		return (ArrayList) cervezaBaseDatos;
	}
/*
	public int getNumeroMarcasCerveza() {

		int marcarCervezas = 0;
		String sql = ConstantesSQL.SQL_SELECT_NUMERO_DE_MARCAS_CERVEZAS;
		conectar();
		Statement st;
		try {
			st = conexion.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				marcarCervezas = rs.getInt(2);
				System.out.println(marcarCervezas + "CONSULTA COUNT");
			}

		} catch (SQLException e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}

		desconectar();
		return marcarCervezas;

	}

	public String[] getMarcasCerveza() {

		// List<Cerveza> cervezaBaseDatos = new ArrayList<Cerveza>();
		String[] marcarCervezas = new String[getNumeroMarcasCerveza()];
		String sql = ConstantesSQL.SQL_SELECT_CERVEZAS;
		conectar();
		Statement st;
		try {
			st = conexion.createStatement();
			ResultSet rs = st.executeQuery(sql);
			int i = 0;
			while (rs.next()) {
				//Cerveza cerveza = new Cerveza();
				marcarCervezas[i] = (rs.getString("marca"));
				i++;			
			}
		} catch (SQLException e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
		desconectar();
		return marcarCervezas;
	}
*/
	public void borrarCerveza(int id) {

		conectar();

		String sql = ConstantesSQL.SQL_DELETE_CERVEZA;
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setInt(1, id);
			ps.execute();

		} catch (SQLException e) {
			System.out.println("ERROR Prepared Statement: " + e.getMessage());
			e.printStackTrace();
		}

		desconectar();

	}

	public void editarCerveza(Cerveza cervezaAeditar) {

		conectar();

		String sql = ConstantesSQL.SQL_UPDATE_CERVEZA;
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setString(1, cervezaAeditar.getMarca());
			ps.setDouble(2, cervezaAeditar.getPrecio());
			ps.setInt(3, cervezaAeditar.getCantidad());
			ps.setString(4, cervezaAeditar.getIngredientes());
			ps.setString(5, cervezaAeditar.getOpinion());
			ps.setString(6, cervezaAeditar.getDescripcion());
			ps.setInt(7, cervezaAeditar.getId());

			ps.execute();

		} catch (SQLException e) {
			System.out.println("ERROR Prepared Statement: " + e.getMessage());
			e.printStackTrace();
		}

		desconectar();
	}

	public boolean guardarCervezasArchivo(File file) {

		try {
			// File file = new File(NOMBRE_ARCHIVO);
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			// for (Cerveza c : cervezas) {
			ArrayList<Cerveza> cervezas = getCervezas();
			oos.writeObject(cervezas);
			// }
			// oos.writeObject(cervezas);
			oos.close();
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			return false;
		}
		return true;
	}

	public boolean guardarCervezasArchivoTexto(File file) {

		try {
			FileWriter fw = new FileWriter(file);
			PrintWriter pw = new PrintWriter(fw);
			ArrayList<Cerveza> cervezasArchivo = getCervezas();

			for (Cerveza c : cervezasArchivo) {
				// fw.write(c.toString() + " \n");
				pw.println(c.toString());
			}
			// fw.close();
			pw.close();
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			return false;
		}
		return true;
	}

	public void borrarTodasLasCervezas() {
		conectar();

		String sql = ConstantesSQL.SQL_BORRAR_TODAS_CERVEZAS;
		try {
			Statement st = conexion.createStatement();
			st.execute(sql);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		desconectar();

	}

	public boolean cargarFichero(File file) {
		// File file = new File(NOMBRE_ARCHIVO);
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);

			ArrayList<Cerveza> cervezasArchivo = (ArrayList<Cerveza>) ois.readObject();

			borrarTodasLasCervezas();

			for (Cerveza cerveza : cervezasArchivo) {

				guardarCerveza(cerveza);
			}
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR: " + e.getMessage());

			return false;
		}
		return true;
	}

	public boolean cargarFicheroTexto(File file) {
		// File file = new File(NOMBRE_ARCHIVO);
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			String lineaCerveza = "";
			String atributos[] = new String[7];

			borrarTodasLasCervezas();

			while ((lineaCerveza = br.readLine()) != null) {

				atributos = lineaCerveza.split(",");
				Cerveza nueva = new Cerveza(atributos[1], Double.parseDouble(atributos[2]),
						Integer.parseInt(atributos[3]), atributos[4], atributos[5], atributos[6]);
				guardarCerveza(nueva);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR: " + e.getMessage());
			return false;
		}
		return true;
	}

	public Cerveza buscarCerveza(int idCerveza) {

		conectar();
		Cerveza cerveza = new Cerveza();
		String sql = ConstantesSQL.SQL_OBTENER_CERVEZA_POR_ID;
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setInt(1, idCerveza);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				cerveza.setId(rs.getInt("id"));
				cerveza.setMarca(rs.getString("marca"));
				cerveza.setPrecio(rs.getDouble("precio"));
				cerveza.setCantidad(rs.getInt("cantidad"));
				cerveza.setIngredientes(rs.getString("ingredientes"));
				cerveza.setOpinion(rs.getString("opinion"));
				cerveza.setDescripcion(rs.getString("descripcion"));
			}
		} catch (SQLException e) {
			System.out.println("ERROR PREPA.STAMENT: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		desconectar();
		return cerveza;
	}

	public void guardarCervezasArchivoExcel(File file) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		workbook.setSheetName(0, "Hoja excel");
		String[] headers = new String[] { "ID", "Marca", "Precio", "Cantidad", "Ingredientes", "Opinion",
				"Descripción" };

		CellStyle headerStyle = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		headerStyle.setFont(font);

		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		HSSFRow headerRow = sheet.createRow(0);
		for (int i = 0; i < headers.length; ++i) {
			String header = headers[i];
			HSSFCell cell = headerRow.createCell(i);
			cell.setCellStyle(headerStyle);
			cell.setCellValue(header);
		}

		ArrayList<Cerveza> cervezasArchivo = new ArrayList<Cerveza>();
		cervezasArchivo = getCervezas();

		for (int i = 0; i < cervezasArchivo.size(); ++i) {
			HSSFRow dataRow = sheet.createRow(i + 1);

			Cerveza c = cervezasArchivo.get(i);
			int id = c.getId();
			String marca = c.getMarca();
			double precio = c.getPrecio();
			int cantidad = c.getCantidad();
			String ingredientes = c.getIngredientes();
			String opinion = c.getOpinion();
			String descripcion = c.getDescripcion();

			dataRow.createCell(0).setCellValue(id);
			dataRow.createCell(1).setCellValue(marca);
			dataRow.createCell(2).setCellValue(precio);
			dataRow.createCell(3).setCellValue(cantidad);
			dataRow.createCell(4).setCellValue(ingredientes);
			dataRow.createCell(5).setCellValue(opinion);
			dataRow.createCell(6).setCellValue(descripcion);
		}

		HSSFRow dataRow = sheet.createRow(1 + cervezasArchivo.size());
		HSSFCell total = dataRow.createCell(1);
		total.setCellStyle(style);

		try {
			FileOutputStream fos = new FileOutputStream(file);
			workbook.write(fos);
			fos.close();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		}

	}

	public void cargaCervezasArchivoExcel(File file) {
		try {

			borrarTodasLasCervezas();
			HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(file));
			HSSFSheet sheet = wb.getSheetAt(0);

			int rows = sheet.getLastRowNum();
			for (int i = 1; i < rows; ++i) {
				HSSFRow row = sheet.getRow(i);

				HSSFCell idCell = row.getCell(0);
				HSSFCell marcaCell = row.getCell(1);
				HSSFCell precioCell = row.getCell(2);
				HSSFCell cantidadCell = row.getCell(3);
				HSSFCell ingredientesCell = row.getCell(4);
				HSSFCell opinionCell = row.getCell(5);
				HSSFCell descripcionCell = row.getCell(6);

				int id = (int) idCell.getNumericCellValue();
				String marca = marcaCell.getStringCellValue();
				double precio = precioCell.getNumericCellValue();
				int cantidad = (int) cantidadCell.getNumericCellValue();
				String ingredientes = ingredientesCell.getStringCellValue();
				String opinion = opinionCell.getStringCellValue();
				String descripcion = descripcionCell.getStringCellValue();

				Cerveza nueva = new Cerveza(id, marca, precio, cantidad, ingredientes, opinion, descripcion);

				guardarCerveza(nueva);
			}
			wb.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void guardarCervezasArchivoPdf(File file) {

		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(file));
			document.open();

			ArrayList<Cerveza> cervezasArchivo = getCervezas();
			Paragraph parrafo = new Paragraph();

			for (Cerveza c : cervezasArchivo) {

				parrafo.add(c.toString() + "\n");
				parrafo.setAlignment(Element.ALIGN_JUSTIFIED);
				// parrafo.breakUp();

			}
			document.add(parrafo);
			document.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		} catch (DocumentException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		}

	}

	public void cargaCervezasArchivoPdf(File file) {

		PdfReader pdfReader;
		try {
			pdfReader = new PdfReader(file.getAbsolutePath());
			int pages = pdfReader.getNumberOfPages();
			borrarTodasLasCervezas();
			// Iterate the pdf through pages.
			for (int i = 1; i <= pages; i++) {
				// Extract the page content using PdfTextExtractor.
				String pageContent = PdfTextExtractor.getTextFromPage(pdfReader, i);
				String[] lineasPagina = pageContent.split("\n");

				for (String linea : lineasPagina) {
					String[] atributosCerveza = linea.split(",");
					Cerveza nueva = new Cerveza(Integer.parseInt(atributosCerveza[0]), atributosCerveza[1],
							Double.parseDouble(atributosCerveza[2]), Integer.parseInt(atributosCerveza[3]),
							atributosCerveza[4], atributosCerveza[5], atributosCerveza[6]);
					guardarCerveza(nueva);
				}
			}
			pdfReader.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}
