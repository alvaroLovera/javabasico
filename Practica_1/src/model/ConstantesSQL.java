package model;

public class ConstantesSQL {
	public static final String SQL_INSERCION_CERVEZA = "insert into cervezas "
			+ "(marca,precio,cantidad,ingredientes,opinion,descripcion) value(?,?,?,?,?,?)";

	public static final String SQL_SELECT_CERVEZAS = "SELECT * FROM cervezas ";

	public static final String SQL_DELETE_CERVEZA = "DELETE FROM cervezas " + "WHERE id = ?";
	
	public static final String SQL_UPDATE_CERVEZA = "UPDATE cervezas SET marca = ?, "
			+ "precio = ?, cantidad = ?, ingredientes = ?, opinion = ?, descripcion = ?  WHERE id = ?";

	public static final String SQL_OBTENER_CERVEZA_POR_ID = "SELECT * FROM cervezas where id = ?";
	
	public static final String SQL_BORRAR_TODAS_CERVEZAS = "DELETE FROM cervezas " + "WHERE id > 0 ";
	
	public static final String SQL_SELECT_MARCA_CERVEZAS = "SELECT marca FROM cervezas ";
	
	public static final String SQL_SELECT_NUMERO_DE_MARCAS_CERVEZAS = "SELECT COUNT(DISTINCT 2) FROM cervezas ";
	
	public static final String SQL_INSERCION_CLIENTE = "insert into cliente "
			+ "(nombre, apellidos, edad, cervezaFavorita, alcoholico) value(?,?,?,?,?)";
	
	public static final String SQL_SELECT_CLIENTES = "SELECT * FROM cliente ";
	
	public static final String SQL_OBTENER_CLIENTE_POR_ID = "SELECT * FROM cliente where id = ?";
	
	
	public static final String SQL_DELETE_CLIENTE = "DELETE FROM cliente " + "WHERE id = ?";
	
	public static final String SQL_UPDATE_CLIENTE = "UPDATE cliente SET nombre = ?, "
			+ "apellidos = ?, edad = ?, cervezaFavorita = ?, alcoholico = ?  WHERE id = ?";
	
	
}
