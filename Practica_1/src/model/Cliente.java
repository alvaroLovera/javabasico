package model;

public class Cliente {
	
	private String nombre;
	private String apellidos;
	private int edad;
	private String cervezaFavorita;
	private boolean alcoholismo;
	private int id;
	
	
	public Cliente() {
		
	}

	public Cliente(String nombre, String apellidos, int edad, String cervezaFavorita, boolean alcoholismo) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.cervezaFavorita = cervezaFavorita;
		this.alcoholismo = alcoholismo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCervezaFavorita() {
		return cervezaFavorita;
	}

	public void setCervezaFavorita(String cervezaFavorita) {
		this.cervezaFavorita = cervezaFavorita;
	}

	public boolean isAlcoholismo() {
		return alcoholismo;
	}

	public void setAlcoholismo(boolean alcoholismo) {
		this.alcoholismo = alcoholismo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
