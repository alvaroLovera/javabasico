package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import model.Cerveza;
import model.CervezaDAO;

public class VentanaPrincipal extends JFrame implements ActionListener {

	PanelInsertarCerveza pInsertarCerveza = new PanelInsertarCerveza();
	PanelListarCervezas pListaCervezas = new PanelListarCervezas(this);
	PanelEditarCerveza pEditarCerveza = new PanelEditarCerveza();
	PanelListarCliente pListarClientes = new PanelListarCliente(this);
	PanelInsertarCliente pInsertarCliente = new PanelInsertarCliente();
	PanelEditarCliente pEditarCliente = new PanelEditarCliente();
	// Barra de Menu
	// JMenu file = new JMenu("File");
	// menubar.add(file);
	// JMenuItem newgame = new JMenuItem("New");
	// file.add(newgame);

	JMenuBar barraMenu = new JMenuBar();
	JMenu menuInsertar = new JMenu("Insertar");
	JMenu menuListar = new JMenu("Listar");
	JMenu menuArchivo = new JMenu("Archivo");
	JMenuItem menuItemInsertarCerveza = new JMenuItem("Cerveza");
	JMenuItem menuItemInsertarCliente = new JMenuItem("Cliente");
	
	JMenuItem menuItemListaCervezas = new JMenuItem("Cervezas");
	JMenuItem menuItemListaClientes = new JMenuItem("Clientes");
	JMenuItem menuItemGuardarArchivo = new JMenuItem("Guardar en Archivo");
	JMenuItem menuItemGuardarArchivoTexto = new JMenuItem("Guardar en Archivo TXT");
	JMenuItem menuItemCargarArchivo = new JMenuItem("Cargar datos desde Archivo");
	JMenuItem menuItemCargarArchivoTexto = new JMenuItem("Cargar datos desde Archivo TXT");
	JMenuItem menuItemGuardarArchivoExcel = new JMenuItem("Guardar en Archivo Excel");
	JMenuItem menuItemCargarArchivoExcel = new JMenuItem("Cargar datos desde Excel");
	JMenuItem menuItemGuardarArchivoPDF = new JMenuItem("Guardar en Archivo PDF");
	JMenuItem menuItemCargarArchivoPDF = new JMenuItem("Cargar datos desde PDF");

	// JTable tabla = new JTable();

	public VentanaPrincipal() {
		// mostrarVentana();
		menuItemInsertarCerveza.setIcon(new ImageIcon("cerveza.png"));

		menuInsertar.add(menuItemInsertarCerveza);
		menuInsertar.add(menuItemInsertarCliente);
		
		menuListar.add(menuItemListaCervezas);
		menuListar.add(menuItemListaClientes);
		menuArchivo.add(menuItemGuardarArchivo);
		menuArchivo.add(menuItemCargarArchivo);
		menuArchivo.add(menuItemGuardarArchivoTexto);
		menuArchivo.add(menuItemCargarArchivoTexto);
		menuArchivo.add(menuItemGuardarArchivoExcel);
		menuArchivo.add(menuItemCargarArchivoExcel);
		menuArchivo.add(menuItemGuardarArchivoPDF);
		menuArchivo.add(menuItemCargarArchivoPDF);

		barraMenu.add(menuInsertar);
		barraMenu.add(menuListar);
		barraMenu.add(menuArchivo);
		setJMenuBar(barraMenu);

		menuItemInsertarCerveza.addActionListener(this);
		menuItemInsertarCliente.addActionListener(this);
		menuItemListaClientes.addActionListener(this);
		
		menuItemListaCervezas.addActionListener(this);
		menuItemGuardarArchivo.addActionListener(this);
		menuItemCargarArchivo.addActionListener(this);
		menuItemGuardarArchivoTexto.addActionListener(this);
		menuItemCargarArchivoTexto.addActionListener(this);
		menuItemGuardarArchivoExcel.addActionListener(this);
		menuItemCargarArchivoExcel.addActionListener(this);
		menuItemGuardarArchivoPDF.addActionListener(this);
		menuItemCargarArchivoPDF.addActionListener(this);

		setSize(900, 600);
		setLocation(300, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setContentPane(pInsertarCerveza);

		this.setIconImage(new ImageIcon(getClass().getResource("/gui/iconoCerveza.jpg")).getImage());

		setVisible(true);
	}

	public void mostrarVentana() {
		setSize(700, 500);
		setLocation(300, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// JLabel Jltitulo = new JLabel();
		// JTextField JtFtitulo = new JTextField();
		// JLabel Jlprecio = new JLabel();
		// JTextField JtFprecio = new JTextField();
//		JLabel Jlduracion = new JLabel();
//		JTextField JtFduracion = new JTextField();	
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {

		// CervezaDAO cervezas = new CervezaDAO();
		Object pulsado = ae.getSource();

		System.out.println("Action Commadn: VN " + ae.getActionCommand());
		// ae.getActionCommand() devuelve el nombre del boton
		if (ae.getActionCommand().equals("EDITAR")) {

			int idAeditar = pListaCervezas.obtenerIdSeleccionado();
			System.out.println(idAeditar);
			setContentPane(pEditarCerveza);
			pEditarCerveza.mostrarCervezaAeditar(idAeditar);
		}else if (ae.getActionCommand().equals("EDITAR Cliente")) {

			int idAeditar = pListarClientes.obtenerIdSeleccionado();
			System.out.println(idAeditar);
			setContentPane(pEditarCliente);
			pEditarCliente.mostrarCLienteAeditar(idAeditar);
		} else if (ae.getActionCommand().equals("BORRAR Cliente")) {

			int idAeditar = pListarClientes.obtenerIdSeleccionado();
			System.out.println(idAeditar);
			setContentPane(pEditarCliente);
			pEditarCliente.mostrarCLienteAeditar(idAeditar);
		} else if (ae.getActionCommand().equals("BORRAR")) {

			int idAeditar = pListaCervezas.obtenerIdSeleccionado();
			System.out.println(idAeditar);
			setContentPane(pEditarCerveza);
			pEditarCerveza.mostrarCervezaAeditar(idAeditar);
		} else if (pulsado == menuItemInsertarCerveza) {
			setContentPane(pInsertarCerveza);
			System.out.println("Has pulsado Insertar");
		}else if (pulsado == menuItemInsertarCliente) {
			setContentPane(pInsertarCliente);
			System.out.println("Has pulsado Insertar Cliente");
		} else if (pulsado == menuItemListaClientes) {
			setContentPane(pListarClientes);;
			pListarClientes.refrescarClientes();

			System.out.println("Has pulsado Listar");
		} else if (pulsado == menuItemListaCervezas) {
			setContentPane(pListaCervezas);
			pListaCervezas.refrescarCervezas();

			System.out.println("Has pulsado Listar");
		} else if (pulsado == menuItemGuardarArchivo) {
			// Voy a presentarle al usuario un selector de archivos
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {

				File file = chooser.getSelectedFile();
				System.out.println("El archivo elegido es " + file.getName());
				CervezaDAO.getInstancia().guardarCervezasArchivo(file);
				JOptionPane.showMessageDialog(this, "Archivo guardado con �xito");
			}
		} else if (pulsado == menuItemCargarArchivo) {
			// Vamos a pedir al usuario que elija el archivo
			JFileChooser chooser = new JFileChooser();
			int returVal = chooser.showOpenDialog(this);

			if (returVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				int input = JOptionPane.showConfirmDialog(this,
						"Se sobreescribiran todos los datos con los del archivo �est� seguro?");
				// 0=yes, 1=no, 2=cancel
				if (input == JOptionPane.OK_OPTION) {
					CervezaDAO.getInstancia().cargarFichero(file);
					pListaCervezas.refrescarCervezas();
				}
			}
		} else if (pulsado == menuItemCargarArchivoTexto) {
			System.out.println("CARGA TXT");
			JFileChooser chooser = new JFileChooser();
			int returVal = chooser.showOpenDialog(this);

			if (returVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				int input = JOptionPane.showConfirmDialog(this,"Se sobreescribiran todos los datos con los del archivo �est� seguro?");
				// 0=yes, 1=no, 2=cancel
				if (input == JOptionPane.OK_OPTION) {
					CervezaDAO.getInstancia().cargarFicheroTexto(file);
					pListaCervezas.refrescarCervezas();
				}
			}
		} else if (pulsado == menuItemGuardarArchivoTexto) {
			// Voy a presentarle al usuario un selector de archivos
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				
				File file = chooser.getSelectedFile();
				System.out.println("El archivo elegido es " + file.getName());
				CervezaDAO.getInstancia().guardarCervezasArchivoTexto(file);
				JOptionPane.showMessageDialog(this, "Archivo guardado con �xito");
			}
		}else if (pulsado == menuItemGuardarArchivoExcel) {
			// Voy a presentarle al usuario un selector de archivos
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				
				File file = chooser.getSelectedFile();
				System.out.println("El archivo elegido es " + file.getName());
				CervezaDAO.getInstancia().guardarCervezasArchivoExcel(file);
				JOptionPane.showMessageDialog(this, "Archivo guardado con �xito");
			}
		}else if (pulsado == menuItemCargarArchivoExcel) {
			// Voy a presentarle al usuario un selector de archivos
			JFileChooser chooser = new JFileChooser();
			int returnVal = JOptionPane.showConfirmDialog(this,
					"Se sobreescribiran todos los datos con los del archivo �est� seguro?");
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				chooser.showOpenDialog(this);
				File file = chooser.getSelectedFile();
				System.out.println("El archivo elegido es " + file.getName());
				CervezaDAO.getInstancia().cargaCervezasArchivoExcel(file);
				JOptionPane.showMessageDialog(this, "Archivo guardado con �xito");
				pListaCervezas.refrescarCervezas();
			}
		}else if (pulsado == menuItemGuardarArchivoPDF) {
			// Voy a presentarle al usuario un selector de archivos
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				
				File file = chooser.getSelectedFile();
				System.out.println("El archivo elegido es " + file.getName());
				CervezaDAO.getInstancia().guardarCervezasArchivoPdf(file);
				JOptionPane.showMessageDialog(this, "Archivo guardado con �xito");
			}
		}else if (pulsado == menuItemCargarArchivoPDF) {
			// Voy a presentarle al usuario un selector de archivos
			JFileChooser chooser = new JFileChooser();
			int returnVal = JOptionPane.showConfirmDialog(this,
					"Se sobreescribiran todos los datos con los del archivo �est� seguro?");
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				chooser.showOpenDialog(this);
				File file = chooser.getSelectedFile();
				System.out.println("El archivo elegido es " + file.getName());
				CervezaDAO.getInstancia().cargaCervezasArchivoPdf(file);
				JOptionPane.showMessageDialog(this, "Archivo guardado con �xito");
				pListaCervezas.refrescarCervezas();
			}
		}

		SwingUtilities.updateComponentTreeUI(this);
	}

	public PanelEditarCerveza getpEditarCerveza() {
		return pEditarCerveza;
	}

}
