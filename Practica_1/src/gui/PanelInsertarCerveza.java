package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import model.Cerveza;
import model.CervezaDAO;
import model.Constantes;

public class PanelInsertarCerveza extends JPanel implements ActionListener {

	GridBagConstraints gbc = new GridBagConstraints();
	GridBagLayout gbl = new GridBagLayout();

	JLabel tituloPanel = new JLabel("Inserta una Cerveza");
	JLabel labelMarca = new JLabel("Marca: ");
	JLabel labelPrecio = new JLabel("Precio: ");
	JLabel labelCantidad = new JLabel("Cantida: ");
	JLabel labelIgredientes = new JLabel("Ingredientes: ");
	JLabel labelopinion = new JLabel("Opini�n: ");
	JLabel labelDescripcion = new JLabel("Descipci�n: ");

	JTextField tFmarca = new JTextField(10);
	JTextField tFopinion = new JTextField(10);
	JComboBox<String> comboCantidad = new JComboBox<String>(Constantes.CANTIDAD);

	JTextArea tAdescripcion = new JTextArea(5, 10);

	// Precio
	JRadioButton rBotonUno = new JRadioButton("1 $");
	JRadioButton rBotonCinco = new JRadioButton("5 $");
	JRadioButton rBotonDiez = new JRadioButton("10 $");
	JRadioButton rBotonQuince = new JRadioButton("15 $");

	// Ingredientes
	JCheckBox checkLupulo = new JCheckBox("L�pulo");
	JCheckBox checkTrigo = new JCheckBox("Trigo");
	JCheckBox checkMalta = new JCheckBox("Malta");
	JCheckBox checkCebada = new JCheckBox("Cebada");

	JButton botonRegistrarCerveza = new JButton("REGISTRAR");

	public PanelInsertarCerveza() {

		setSize(1000, 1000);

		ButtonGroup bg = new ButtonGroup();
		bg.add(rBotonUno);
		bg.add(rBotonCinco);
		bg.add(rBotonDiez);
		bg.add(rBotonQuince);

		setLayout(gbl);
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(tituloPanel, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		add(labelMarca, gbc);
		// segunda fila
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(tFmarca, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		add(labelPrecio, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		add(rBotonUno, gbc);

		gbc.gridx = 1;
		gbc.gridy = 3;
		add(rBotonCinco, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		add(rBotonDiez, gbc);

		gbc.gridx = 1;
		gbc.gridy = 5;
		add(rBotonQuince, gbc);

		// tercera fila
		gbc.gridx = 0;
		gbc.gridy = 7;
		add(labelCantidad, gbc);
		gbc.gridx = 1;
		gbc.gridy = 7;
		add(comboCantidad, gbc);
		// cuarta fila
		gbc.gridx = 0;
		gbc.gridy = 8;
		// gbc.gridwidth = 2;
		add(labelIgredientes, gbc);

		gbc.gridx = 0;
		gbc.gridy = 9;
		add(checkLupulo, gbc);

		gbc.gridx = 1;
		gbc.gridy = 9;
		add(checkTrigo, gbc);

		gbc.gridx = 0;
		gbc.gridy = 10;
		add(checkMalta, gbc);

		gbc.gridx = 1;
		gbc.gridy = 10;
		add(checkCebada, gbc);

		gbc.gridx = 0;
		gbc.gridy = 11;
		add(labelopinion, gbc);

		gbc.gridx = 1;
		gbc.gridy = 11;
		add(tFopinion, gbc);

		gbc.gridx = 0;
		gbc.gridy = 12;
		add(labelDescripcion, gbc);

		gbc.gridx = 1;
		gbc.gridy = 14;
		add(tAdescripcion, gbc);

		gbc.gridx = 0;
		gbc.gridy = 15;
		gbc.gridwidth = 2;
		add(botonRegistrarCerveza, gbc);

		botonRegistrarCerveza.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// Object pulsado = ae.getSource();

		String marca = tFmarca.getText();
		Double precio = 0.0;
		String ingrediente = "";
		ArrayList<String> ingredientesCheckBox = new ArrayList<String>();
		String cantidadString = comboCantidad.getSelectedItem().toString();
		int cantidadInt = 0;
		String descripcion = tAdescripcion.getText().trim();
		String opinion = tFopinion.getText().trim();

		if (marca.trim().isEmpty()) {

			JOptionPane.showMessageDialog(this, "Rellene el campo Marca");
			return;
		} 
		if (!rBotonUno.isSelected() && !rBotonCinco.isSelected() && !rBotonDiez.isSelected()
				&& !rBotonQuince.isSelected()) {

			JOptionPane.showMessageDialog(this, "Seleccione un precio");
			return;
		}

		if (rBotonUno.isSelected()) {
			precio = 1.0;
		} else if (rBotonCinco.isSelected()) {
			precio = 5.0;
		} else if (rBotonDiez.isSelected()) {
			precio = 10.0;
		} else if (rBotonQuince.isSelected()) {
			precio = 15.0;
		}

		if (!checkLupulo.isSelected() && !checkTrigo.isSelected() && !checkMalta.isSelected()
				&& !checkCebada.isSelected()) {

			JOptionPane.showMessageDialog(this, "Seleccione al menos un ingrediente");
			return;
		}

		if (checkLupulo.isSelected()) {
			ingrediente = ingrediente + Constantes.INGREDIENTES[1];
		}
		if (checkTrigo.isSelected()) {
			ingrediente = ingrediente + Constantes.INGREDIENTES[2];
		}
		if (checkMalta.isSelected()) {
			ingrediente = ingrediente + Constantes.INGREDIENTES[0];
		}
		if (checkCebada.isSelected()) {
			ingrediente = ingrediente + Constantes.INGREDIENTES[3];
		}

		if (opinion.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Rellene el campo Opini�n");
			return;
		}

		if (descripcion.isEmpty() && descripcion.length() < 2 && descripcion.length() > 100) {
			JOptionPane.showMessageDialog(this, "Rellene la descripcion");
			return;
		}

		if (cantidadString.equals(Constantes.CANTIDAD[0])) {
			cantidadInt = 1;
		} else if (cantidadString.equals(Constantes.CANTIDAD[1])) {
			cantidadInt = 6;
		} else if (cantidadString.equals(Constantes.CANTIDAD[2])) {
			cantidadInt = 12;
		} else if (cantidadString.equals(Constantes.CANTIDAD[3])) {
			cantidadInt = 20;
		}

		
		Cerveza nueva = new Cerveza(marca, precio, cantidadInt,
				ingrediente,opinion, descripcion);
		//CervezaDAO cervezas = new CervezaDAO();		
		
		CervezaDAO.getInstancia().guardarCerveza(nueva);
		JOptionPane.showMessageDialog(this, "Registrado correctamente");
		
		SwingUtilities.updateComponentTreeUI(this);
		
	}


}
