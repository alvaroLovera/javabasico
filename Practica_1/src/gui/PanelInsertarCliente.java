package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import model.CervezaDAO;
import model.Cliente;
import model.ClienteDAO;
import model.Constantes;

public class PanelInsertarCliente extends JPanel implements ActionListener {

	GridBagConstraints gbc = new GridBagConstraints();
	GridBagLayout gbl = new GridBagLayout();

	JLabel tituloPanel = new JLabel("Registro de Clientes");
	JLabel labelNombre = new JLabel("Nombre: ");
	JLabel labelApellidos = new JLabel("Apellidos: ");
	JLabel labelEdad = new JLabel("Edad: ");
	JLabel labelCervezaFavorita = new JLabel("Cerveza: ");
	JLabel labelAlcoholico = new JLabel("�Es usted alc�holico?");

	JTextField tFnombre = new JTextField(10);
	JTextField tFapellidos = new JTextField(10);
	JTextField tFedad = new JTextField(3);
	
	String [] cervezasFavoritas = {"Coronita", "Estrella Galicia", "Cruzacampo"};
	JComboBox<String> comboCervezasFavoritas = new JComboBox<String>(cervezasFavoritas);
	
	JRadioButton rBotonSi = new JRadioButton("Si");
	JRadioButton rBotonNo = new JRadioButton("No");

	JButton botonRegistrarCliente = new JButton("REGISTRAR");

	ButtonGroup bg = new ButtonGroup();

	public PanelInsertarCliente() {

		setSize(600, 600);

		bg.add(rBotonSi);
		bg.add(rBotonNo);

		setLayout(gbl);
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(tituloPanel, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		add(labelNombre, gbc);
		// segunda fila
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(tFnombre, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		add(labelApellidos, gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		add(tFapellidos, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		add(labelEdad, gbc);

		gbc.gridx = 1;
		gbc.gridy = 3;
		add(tFedad, gbc);

		gbc.gridx = 0;
		gbc.gridy = 4;
		add(labelCervezaFavorita, gbc);

		gbc.gridx = 1;
		gbc.gridy = 4;
		add(comboCervezasFavoritas, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		add(labelAlcoholico, gbc);

		gbc.gridx = 1;
		gbc.gridy = 5;
		add(rBotonSi, gbc);

		gbc.gridx = 1;
		gbc.gridy = 6;
		add(rBotonNo, gbc);

		gbc.gridx = 0;
		gbc.gridy = 7;
		gbc.gridwidth = 2;
		add(botonRegistrarCliente, gbc);

		botonRegistrarCliente.addActionListener(this);

		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String nombre = tFnombre.getText().trim();
		String apellidos = tFapellidos.getText().trim();
		int edad;
		String cervezaFavorita;
		boolean alcoholismo = false;
		
		try {
			edad = Integer.parseInt(tFedad.getText().trim());
			if(tFedad.getText().trim().isEmpty()) {
				JOptionPane.showMessageDialog(this, "Rellene el campo edad, debe ser +18");
				return;
		
			
			}
		}catch (NumberFormatException nfe) {
			
			JOptionPane.showMessageDialog(this, "En el campo edad solo pueda introducir n�meros");
			return;
		}
		
		
		cervezaFavorita = comboCervezasFavoritas.getSelectedItem().toString();
		
		

		if (nombre.isEmpty()||nombre.length() <3) {
			JOptionPane.showMessageDialog(this, "Rellene el campo nombre, con m�s de 3 digitos");
			return;
		} 
		if (!rBotonNo.isSelected() && !rBotonSi.isSelected()) {
			JOptionPane.showMessageDialog(this, "Seleccione un precio");
			return;
		}else if (rBotonNo.isSelected()){
			alcoholismo = false;
		}else if (rBotonSi.isSelected()){
			alcoholismo = true;
		}
		
		if(tFedad.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Rellene el campo edad, debe ser +18");
			return;
		}else if(edad < 18){
			JOptionPane.showMessageDialog(this, "Rellene el campo edad, debe ser +18");
			return;
		}else{
			edad = Integer.parseInt(tFedad.getText().trim());
		}
		
		
		Cliente cli = new Cliente(nombre,apellidos,edad,cervezaFavorita,alcoholismo);
		
		ClienteDAO clienteDAO = new ClienteDAO();
		clienteDAO.guardarCliente(cli);
		JOptionPane.showMessageDialog(this, "Se ha registrado con �xito");
		
	}

}
