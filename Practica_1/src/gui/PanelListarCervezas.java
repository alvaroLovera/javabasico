package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import model.Cerveza;
import model.CervezaDAO;

import javax.swing.JScrollPane;

public class PanelListarCervezas extends JPanel implements ActionListener{

	//JLabel titulo = new JLabel("Listado de Cervezas");
	private ArrayList<Cerveza> cervezas;
	private TmCervezas modelo;
	private JTable tabla;
	private JScrollPane scrollpane;
	private String[] columnNames = { "Marca", "Precio", "Cantidad", "Opini�n", "Descripci�n","Ingrediente","ID" };
	private DefaultTableModel dtm;
	private JButton botonBorrar;
	private JButton botonEditar;
	private JScrollPane barraDesplaz;
	private PanelEditarCerveza editarCerveza;
	private VentanaPrincipal ventanaPrincipal;
	// se crea la Tabla con el modelo DefaultTableModel
	// una vez creada la tabla con su modelo
	// podemos agregar columnas

	
	//EN el constructor exigimos la VetanaPrincipal la cual usaremos para decirle al botn editar que
	//qure se a el elemento que atienda su click
	public PanelListarCervezas(VentanaPrincipal ventanaPrincipal) {

		this.ventanaPrincipal = ventanaPrincipal;
		tabla = new JTable(modelo);
		// aunque mi table model no sea nada la tabla ya lo tiene asignado y lo que
		// meta en mi table sera lo que muestre la tabla
		botonBorrar = new JButton("BORRAR");
		botonEditar = new JButton("EDITAR");
		botonEditar.setActionCommand("EDITAR");

		tabla.setPreferredScrollableViewportSize(new Dimension(400, 80));
		tabla.setFillsViewportHeight(true);
		barraDesplaz = new JScrollPane(tabla);

		botonBorrar.addActionListener(this);
		botonEditar.addActionListener(ventanaPrincipal);
		
		//add(titulo);
		add(barraDesplaz);
		add(botonBorrar);
		add(botonEditar);
		
	}
	

	public class TmCervezas extends AbstractTableModel {	

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		
		/*
		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
			
			
			super.setValueAt(aValue, rowIndex, columnIndex);
		}
*/


		@Override
		public String getColumnName(int column) {
			return columnNames[column];
		}

		@Override
		public int getRowCount() {
			return cervezas.size();
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			if (col < 2) {
				return false;
			} else {
				return true;
			}
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			Cerveza cerveza = cervezas.get(rowIndex);
			String info=null;
			
			switch (columnIndex) {
			case 0:
				info = cerveza.getMarca();
				break;
			case 1:
				info = String.valueOf(cerveza.getPrecio());
				break;
			case 2:
				info = String.valueOf(cerveza.getCantidad());
				break;
			case 3:
				info = cerveza.getOpinion();
				break;
			case 4:
				info = cerveza.getDescripcion();
				break;
			case 5:
				info = cerveza.getIngredientes();
				break;	
			case 6: 
				info = String.valueOf(cerveza.getId());
				break;
			default:
				return info;
			}
			return info;
			
		}

	
	}
	public void refrescarCervezas() {

		cervezas = CervezaDAO.getInstancia().getCervezas();
		modelo = new TmCervezas();
		tabla.setModel(modelo);
		// Una vez sacados lo libros que hay y una vez sacados los libros que hay
		// le digo a la tabla que se refreque
		modelo.fireTableDataChanged();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Object botonPulsado =  e.getSource();

		System.out.println(tabla.getSelectedRow());
		if(botonPulsado == botonBorrar && tabla.getSelectedRow() != -1) {
			System.out.println(tabla.getSelectedRow());	
			
	//		JOptionPane.showConfirmDialog(this, "Seguro que desea borrarlo");
			
			String iDseleccionado = (String)
			tabla.getModel().getValueAt( tabla.getSelectedRow(), 6);
			
			 int input = JOptionPane.showConfirmDialog(this, "�Seguro que desea borrarla?");
		        // 0=yes, 1=no, 2=cancel
			if(input == JOptionPane.OK_OPTION) {
			 CervezaDAO.getInstancia().borrarCerveza(Integer.parseInt(iDseleccionado));
			 refrescarCervezas();
			//TmCervezas dtm = (TmCervezas) tabla.getModel(); //TableProducto es el nombre de mi tabla ;) 
			}
			}else  if(botonPulsado == botonBorrar){
				JOptionPane.showMessageDialog(this, "Seleccione la fila que desea borrar");
			}
		
		if(botonPulsado == botonEditar) {
			//TmCervezas dtm = (TmCervezas) tabla.getModel(); //TableProducto es el nombre de mi tabla ;) 
			//dtm.romremoveRow(tabla.getSelectedRow()); 
			if(tabla.getSelectedRow() > -1) {
			//	int indiceSeleccionado = tabla.getSelectedRow();
				
				
				int iDseleccionado = obtenerIdSeleccionado();
				
				//!!!!!!!!		CervezaDAO.getInstancia().editarCerveza();
				System.out.println(iDseleccionado);
		//		this.ventanaPrincipal.getpEditarCerveza().mostrarCervezaAeditar(idCervezaAeditar);
				
				
			
				//Cerveza aEditar =  cervezas.get(tabla.getSelectedRow());
				
				
			}else {
				JOptionPane.showMessageDialog(this, "Seleccione la fila que desea editar");
			}
		}
		
	}

	public int obtenerIdSeleccionado() {
		String iDseleccionado = (String)
				tabla.getModel().getValueAt( tabla.getSelectedRow(), 6);
		return Integer.parseInt(iDseleccionado);
	}

}
