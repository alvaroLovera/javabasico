package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import gui.PanelListarCervezas.TmCervezas;
import model.Cerveza;
import model.CervezaDAO;
import model.Cliente;
import model.ClienteDAO;

public class PanelListarCliente extends JPanel implements ActionListener{

	//JLabel titulo = new JLabel("Listado de Cervezas");
	private ArrayList<Cliente> clientes;
	private TmCervezas modelo;
	private JTable tabla;
	private JScrollPane scrollpane;
	private String[] columnNames = { "Nombre", "Apellidos", "Edad", "Favorita", "Alcoholico","ID" };
	private DefaultTableModel dtm;
	private JButton botonBorrarCliente;
	private JButton botonEditarCliente;
	private JScrollPane barraDesplaz;
	private PanelEditarCerveza editarCliente;
	private VentanaPrincipal ventanaPrincipal;
	// se crea la Tabla con el modelo DefaultTableModel
	// una vez creada la tabla con su modelo
	// podemos agregar columnas

	//EN el constructor exigimos la VetanaPrincipal la cual usaremos para decirle al botn editar que
	//qure se a el elemento que atienda su click
	public PanelListarCliente(VentanaPrincipal ventanaPrincipal) {

		this.ventanaPrincipal = ventanaPrincipal;
		tabla = new JTable(modelo);
		// aunque mi table model no sea nada la tabla ya lo tiene asignado y lo que
		// meta en mi table sera lo que muestre la tabla
		botonBorrarCliente = new JButton("BORRAR Cliente");
		botonEditarCliente = new JButton("EDITAR Cliente");
		botonEditarCliente.setActionCommand("EDITAR Cliente");

		tabla.setPreferredScrollableViewportSize(new Dimension(400, 80));
		tabla.setFillsViewportHeight(true);
		barraDesplaz = new JScrollPane(tabla);

		botonBorrarCliente.addActionListener(this);
		botonEditarCliente.addActionListener(ventanaPrincipal);
		
		//add(titulo);
		add(barraDesplaz);
		add(botonBorrarCliente);
		add(botonEditarCliente);
		
	}
	

	public class TmCervezas extends AbstractTableModel {	

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public String getColumnName(int column) {
			return columnNames[column];
		}

		@Override
		public int getRowCount() {
			return clientes.size();
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			if (col < 2) {
				return false;
			} else {
				return true;
			}
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			Cliente cliente = clientes.get(rowIndex);
			String info=null;
			
			switch (columnIndex) {
			case 0:
				info = cliente.getNombre();
				break;
			case 1:
				info = cliente.getApellidos();
				break;
			case 2:
				info = String.valueOf(cliente.getEdad());
				break;
			case 3:
				info = cliente.getCervezaFavorita();
				break;
			case 4:
				info = String.valueOf(cliente.isAlcoholismo());
				break;
			case 5: 
				info = String.valueOf(cliente.getId());
				break;
			default:
				return info;
			}
			return info;
			
		}

	
	}
	public void refrescarClientes() {
		
		 ClienteDAO clienteDaO = new ClienteDAO();
		clientes = clienteDaO.getClientes();
		modelo = new TmCervezas();
		tabla.setModel(modelo);
		// Una vez sacados lo libros que hay y una vez sacados los libros que hay
		// le digo a la tabla que se refreque
		modelo.fireTableDataChanged();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Object botonPulsado =  e.getSource();

		System.out.println(tabla.getSelectedRow());
		if(botonPulsado == botonBorrarCliente && tabla.getSelectedRow() != -1) {
			System.out.println(tabla.getSelectedRow());	
			
	//		JOptionPane.showConfirmDialog(this, "Seguro que desea borrarlo");
			
			String iDseleccionado = (String)
			tabla.getModel().getValueAt( tabla.getSelectedRow(), 5);
			
			 int input = JOptionPane.showConfirmDialog(this, "�Seguro que desea borrarlo?");
		        // 0=yes, 1=no, 2=cancel
			if(input == JOptionPane.OK_OPTION) {
			// CervezaDAO.getInstancia().borrarCerveza(Integer.parseInt(iDseleccionado));
			ClienteDAO clienteDAO = new ClienteDAO();
			clienteDAO.borrarCliente(Integer.parseInt(iDseleccionado));
			 refrescarClientes();
			//TmCervezas dtm = (TmCervezas) tabla.getModel(); //TableProducto es el nombre de mi tabla ;) 
			}
			}else  if(botonPulsado == botonBorrarCliente){
				JOptionPane.showMessageDialog(this, "Seleccione la fila que desea borrar");
			}
		
		if(botonPulsado == botonEditarCliente) {
			//TmCervezas dtm = (TmCervezas) tabla.getModel(); //TableProducto es el nombre de mi tabla ;) 
			//dtm.romremoveRow(tabla.getSelectedRow()); 
			if(tabla.getSelectedRow() > -1) {
			//	int indiceSeleccionado = tabla.getSelectedRow();
				
				int iDseleccionado = obtenerIdSeleccionado();			
				//!!!!!!!!		CervezaDAO.getInstancia().editarCerveza();
				System.out.println(iDseleccionado);
		//		this.ventanaPrincipal.getpEditarCerveza().mostrarCervezaAeditar(idCervezaAeditar);
				//Cerveza aEditar =  cervezas.get(tabla.getSelectedRow());			
			}else {
				JOptionPane.showMessageDialog(this, "Seleccione la fila que desea editar");
			}
		}
		
	}

	public int obtenerIdSeleccionado() {
		String iDseleccionado = (String)
				tabla.getModel().getValueAt( tabla.getSelectedRow(), 5);
		return Integer.parseInt(iDseleccionado);
	}

}
