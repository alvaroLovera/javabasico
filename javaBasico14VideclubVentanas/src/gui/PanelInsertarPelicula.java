package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.Constantes;
import model.Pelicula;
import model.PeliculasDAO;

public class PanelInsertarPelicula extends JPanel implements ActionListener{
	
	GridBagConstraints gbc = new GridBagConstraints();
	GridBagLayout gbl = new GridBagLayout();
	
	JLabel labelTitulo = new JLabel("Titulo: ");
	JLabel labelDuracion = new JLabel("Duración: ");
	JLabel labelPrecio = new JLabel("Precio: ");
	JLabel labelFormato = new JLabel("Formato ");
	JLabel labelDesccripcion = new JLabel("Desccripción: ");
	
	JTextField textFieldtitulo = new JTextField(10);
	JTextField textFieldduracion = new JTextField(10);
	JTextField textFieldprecio = new JTextField(10);
	JComboBox<String> comboFormato = new JComboBox<String>(Constantes.FORMATOS);
	JTextArea textAreaDescricion = new JTextArea(5,10);
	JButton botonRegistrarPelicula = new JButton("REGISTRAR");
	
	public PanelInsertarPelicula() {
		setLayout(gbl);
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(labelTitulo,gbc);
		gbc.gridx = 1;
		gbc.gridy = 0;
		add(textFieldtitulo,gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(labelDuracion,gbc);
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(textFieldduracion,gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(labelPrecio,gbc);
		gbc.gridx = 1;
		gbc.gridy = 2;
		add(textFieldprecio,gbc);
		gbc.gridx = 0;
		gbc.gridy = 3;
		add(labelFormato,gbc);
		gbc.gridx = 1;
		gbc.gridy = 3;
		add(comboFormato,gbc);
		gbc.gridx = 0;
		gbc.gridy = 4;
		add(labelDesccripcion,gbc);
		gbc.gridx = 1;
		gbc.gridy = 5;
		add(textAreaDescricion,gbc);
		gbc.gridx = 0;
		gbc.gridy = 10;
		gbc.gridwidth = 2;
		add(botonRegistrarPelicula,gbc);
		botonRegistrarPelicula.addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		
		if(validTituloPelicula(textFieldtitulo.getText())&&
				!textFieldduracion.getText().trim().isEmpty()&&
				!textFieldprecio.getText().trim().isEmpty()&&
				!textAreaDescricion.getText().trim().isEmpty()&&
				textAreaDescricion.getText().trim().length()>=10 &&
				textAreaDescricion.getText().trim().length()<=100) {
			
			String titulo = textFieldtitulo.getText().trim();
			int duracion = 0;
			try {
				duracion = Integer.parseInt(textFieldduracion.getText().trim());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Formato de duracion incorrecto");
				return;
			}
			double precio = 0.0;
			try {
				precio = Double.parseDouble(textFieldprecio.getText().trim());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Formato de precio incorrecto");
				return;
			}
			String formato = comboFormato.getSelectedItem().toString();
			String descripcion = textAreaDescricion.getText().trim();
			
			Pelicula nueva = new Pelicula(titulo, precio, duracion, formato, descripcion);
			System.out.println(nueva.getTitulo()+" "+nueva.getPrecio()+" "+nueva.getDuracion()
			+" "+nueva.getFormato()+" "+nueva.getDescripcion());
			
			PeliculasDAO peliDao = new PeliculasDAO();
			peliDao.guardarPelicula(nueva);
			
			JOptionPane.showMessageDialog(this, "Pelicula guardada con exito");
		}else {
			JOptionPane.showMessageDialog(this, "Debe rellenar todos los campos");
		}
		
		
		System.out.println("Boton pulsado");
	}
	
	public boolean validTituloPelicula(String tituloPelicula) {
		boolean isValid = false;
		if(tituloPelicula.trim().isEmpty()||tituloPelicula.trim().length()<2 ||
				tituloPelicula.trim().length()>20) {
			
			isValid = false;
		}else {
			isValid = true;
		}
		return isValid;
	}
	/*
	public boolean validDuracionPelicula(String duracionPelicula) {
		boolean isValid = false;
		if(duracionPelicula.trim().isEmpty()) {
			
			isValid = false;
		}else {
			//Integer.parseInt(duracionPelicula);
			isValid = true;
		}
		return isValid;
	}
	*/
}
