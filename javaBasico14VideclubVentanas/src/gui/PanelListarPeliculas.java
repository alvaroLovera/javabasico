package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import model.Pelicula;
import model.PeliculasDAO;

public class PanelListarPeliculas extends JPanel{

	GridBagConstraints gbc = new GridBagConstraints();
	GridBagLayout gbl = new GridBagLayout();
	JLabel labelIntertar = new JLabel("Listar Peliculones LOKO");
	JList<String> jListPeliculas = new JList<String>();
	DefaultListModel<String> modelo = new DefaultListModel();
	
	public PanelListarPeliculas() {
		PeliculasDAO pelisDAO = new PeliculasDAO();
		jListPeliculas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
		for(Pelicula p : pelisDAO.getPeliculas()) {
			modelo.addElement(p.getTitulo()+" "+p.getDescripcion()+" ");
		}
		jListPeliculas.setModel(modelo);
		
		add(jListPeliculas);
		
	}
	
	public void refrescarJlistPeliculas() {
		PeliculasDAO pelisDAO = new PeliculasDAO();
		for(Pelicula p : pelisDAO.getPeliculas()) {
			modelo.addElement(p.getTitulo()+" "+p.getDescripcion()+" ");
		}
		jListPeliculas.setModel(modelo);
		//add(jListPeliculas);
	}
}
