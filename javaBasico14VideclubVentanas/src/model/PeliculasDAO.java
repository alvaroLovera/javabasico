package model;

import java.awt.List;
import java.util.ArrayList;

public class PeliculasDAO {
	
	private static ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
	
	public void guardarPelicula(Pelicula p) {
		peliculas.add(p);
	}
	
	public ArrayList<Pelicula> getPeliculas() {
		return peliculas;
	}
}
