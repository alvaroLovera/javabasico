package model;

public class Pelicula {

	private String titulo;
	private double precio;
	private int duracion;
	private String formato;
	private String descripcion;
	
	public Pelicula(String titulo, double precio, int duracion, String formato, String descripcion) {
		this.titulo = titulo;
		this.precio = precio;
		this.duracion = duracion;
		this.formato = formato;
		this.descripcion = descripcion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}
	
	
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean formatoIsValid(String formato) {
		boolean exito = false;
		if(formato.equalsIgnoreCase("DVD")||
				formato.equalsIgnoreCase("BLUERAY")||
				formato.equalsIgnoreCase("DESCARGA-DIGITAL")) {
			exito = true;
		}	
		return exito;
	}

}
