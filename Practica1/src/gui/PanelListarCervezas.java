package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import model.Cerveza;
import model.CervezasDAO;

public class PanelListarCervezas extends JPanel{

	GridBagConstraints gbc = new GridBagConstraints();
	GridBagLayout gbl = new GridBagLayout();
	JLabel labelIntertar = new JLabel("Listar Cervezas");
	JList<String> jListCervezas = new JList<String>();
	DefaultListModel<String> modelo = new DefaultListModel();
	
	public PanelListarCervezas() {
		CervezasDAO pelisDAO = new CervezasDAO();
		jListCervezas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
		for(Cerveza p : pelisDAO.getCervezas()) {
			modelo.addElement(p.getNombre()+" "+p.getDescripcion()+" ");
		}
		jListCervezas.setModel(modelo);
		
		add(jListCervezas);
		
	}
	
	public void refrescarJlistPeliculas() {
		CervezasDAO pelisDAO = new CervezasDAO();
		for(Cerveza p : pelisDAO.getCervezas()) {
			modelo.addElement(p.getNombre()+" "+p.getDescripcion()+" ");
		}
		jListCervezas.setModel(modelo);
		//add(jListPeliculas);
	}
}
