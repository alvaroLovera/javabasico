package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;


public class VentanaPrincipal extends JFrame implements ActionListener{
	
	PanelInsertarPelicula pInsertarPelicula = new PanelInsertarPelicula();
	PanelListarCervezas pListaPeliculas = new PanelListarCervezas();
	// Barra de Menu
	JMenuBar barraMenu = new JMenuBar();
	JMenu menuInsertar = new JMenu("Insertar");
	JMenu menuListar = new JMenu("Listar");
	JMenuItem menuItemInsertarPelicula = new JMenuItem("Pelicula");
	JMenuItem menuItemListarPeliculas = new JMenuItem("Peliculas");
	
	public VentanaPrincipal() {	
		//mostrarVentana();
		
		menuInsertar.add(menuItemInsertarPelicula);
		menuListar.add(menuItemListarPeliculas);
		
		barraMenu.add(menuInsertar);
		barraMenu.add(menuListar);
		setJMenuBar(barraMenu);
		
		menuItemInsertarPelicula.addActionListener(this);
		menuItemListarPeliculas.addActionListener(this);
		
		setSize(700, 500);
		setLocation(300,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setContentPane(pInsertarPelicula);
		
		setVisible(true);
	}

	public void mostrarVentana() {
		setSize(700, 500);
		setLocation(300,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//	JLabel Jltitulo = new JLabel();
	//	JTextField JtFtitulo = new JTextField();
	//	JLabel Jlprecio = new JLabel();
	//	JTextField JtFprecio = new JTextField();
//		JLabel Jlduracion = new JLabel();
//		JTextField JtFduracion = new JTextField();	
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		
		JMenuItem pulsado = (JMenuItem)	ae.getSource();
		if(pulsado == menuItemInsertarPelicula) {
			setContentPane(pInsertarPelicula);
			System.out.println("Has pulsado Insertar");
		}
		if(pulsado == menuItemListarPeliculas) {
			setContentPane(pListaPeliculas);
			pListaPeliculas.refrescarJlistPeliculas();
			System.out.println("Has pulsado Listar");
		}
		
		SwingUtilities.updateComponentTreeUI(this);
	}
	
}
