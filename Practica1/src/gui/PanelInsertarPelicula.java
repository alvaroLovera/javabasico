package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.Constantes;
import model.Cerveza;
import model.CervezasDAO;

public class PanelInsertarPelicula extends JPanel implements ActionListener{
	
	GridBagConstraints gbc = new GridBagConstraints();
	GridBagLayout gbl = new GridBagLayout();
	
	JLabel labelTitulo = new JLabel("Nombre: ");
	JLabel labelDuracion = new JLabel("Cantidad: ");
	JLabel labelGraduacion = new JLabel("Graduación: ");
	JLabel labelTipo = new JLabel("Tipo: ");
	JLabel labelIngredientes = new JLabel("Ingredientes: ");
	JLabel labelDesccripcion = new JLabel("Descripción: ");
//	
//	String nombre;
//	private double graduacion;
//	private int cantidad;
//	private String tipo;
//	private String[] ingredientes;
//	private String descripcion;
	
	JTextField textFieldNombre = new JTextField(10);
	JTextField textFieldCantidad = new JTextField(10);
	JTextField textFieldGraduacion = new JTextField(10);
	JComboBox<String> comboIngredientes = new JComboBox<String>(Constantes.INGREDIENTES);
	JTextArea textAreaDescricion = new JTextArea(5,10);
	JButton botonRegistrarCerveza = new JButton("REGISTRAR");
	
	public PanelInsertarPelicula() {
		setLayout(gbl);
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(labelTitulo,gbc);
		gbc.gridx = 1;
		gbc.gridy = 0;
		add(textFieldNombre,gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(labelDuracion,gbc);
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(textFieldCantidad,gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(labelGraduacion,gbc);
		gbc.gridx = 1;
		gbc.gridy = 2;
		add(textFieldGraduacion,gbc);
		gbc.gridx = 0;
		gbc.gridy = 3;
		add(labelTipo,gbc);
		gbc.gridx = 1;
		gbc.gridy = 3;
		add(comboIngredientes,gbc);
		gbc.gridx = 0;
		gbc.gridy = 4;
		add(labelDesccripcion,gbc);
		gbc.gridx = 1;
		gbc.gridy = 5;
		add(textAreaDescricion,gbc);
		gbc.gridx = 0;
		gbc.gridy = 10;
		gbc.gridwidth = 2;
		add(botonRegistrarCerveza,gbc);
		botonRegistrarCerveza.addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		
		if(validTituloPelicula(textFieldNombre.getText())&&
				!textFieldCantidad.getText().trim().isEmpty()&&
				!textFieldGraduacion.getText().trim().isEmpty()&&
				!textAreaDescricion.getText().trim().isEmpty()&&
				textAreaDescricion.getText().trim().length()>=10 &&
				textAreaDescricion.getText().trim().length()<=100) {
			
			String nombre = textFieldNombre.getText().trim();
			int cantidad = 0;
			try {
				cantidad = Integer.parseInt(textFieldCantidad.getText().trim());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Formato de duracion incorrecto");
				return;
			}
			double graduacion = 0.0;
			try {
				graduacion = Double.parseDouble(textFieldGraduacion.getText().trim());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Formato de precio incorrecto");
				return;
			}
			String tipo []= comboIngredientes.getSelectedItem().toString();
			String descripcion = textAreaDescricion.getText().trim();
			
			Cerveza nueva = new Cerveza(nombre, cantidad, graduacion,  tipo, descripcion);
			System.out.println(nueva.getNombre()+" "+nueva.getGraduacion()+" "+nueva.getIngredientes().toString()
			+" "+nueva.getTipo()+" "+nueva.getDescripcion());
			
			CervezasDAO peliDao = new CervezasDAO();
			peliDao.guardarCerveza(nueva);
			
			JOptionPane.showMessageDialog(this, "Pelicula guardada con exito");
		}else {
			JOptionPane.showMessageDialog(this, "Debe rellenar todos los campos");
		}
		
		
		System.out.println("Boton pulsado");
	}
	
	public boolean validTituloPelicula(String tituloPelicula) {
		boolean isValid = false;
		if(tituloPelicula.trim().isEmpty()||tituloPelicula.trim().length()<2 ||
				tituloPelicula.trim().length()>20) {
			
			isValid = false;
		}else {
			isValid = true;
		}
		return isValid;
	}
	/*
	public boolean validDuracionPelicula(String duracionPelicula) {
		boolean isValid = false;
		if(duracionPelicula.trim().isEmpty()) {
			
			isValid = false;
		}else {
			//Integer.parseInt(duracionPelicula);
			isValid = true;
		}
		return isValid;
	}
	*/
}
