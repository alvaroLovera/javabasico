package model;

import java.awt.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class CervezasDAO {
	
	private static ArrayList<Cerveza> cervezas = new ArrayList<Cerveza>();
	private static final String NOMBRE_ARCHIVO = "cervezas.dat";
	
	public boolean guardarCerveza(Cerveza p) {
		
		try {
			File file = new File(NOMBRE_ARCHIVO);
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			cervezas.add(p);
			oos.writeObject(cervezas);
			
			oos.close();
			return true;
			} catch (Exception e) {
				System.out.println("ERROR " + e.getMessage());
				return false;
			}
	}
	
public boolean guardarCervezaArchivo() {
		
		try {
			File file = new File(NOMBRE_ARCHIVO);
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(cervezas);
			
			oos.close();
			return true;
			} catch (Exception e) {
				System.out.println("ERROR " + e.getMessage());
				return false;
			}
	}
	
	public boolean cargarCervezasArchivo() {
		
		try {
			File file = new File(NOMBRE_ARCHIVO);
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Cerveza cerveza = null;	
			do {				
				cerveza = (Cerveza) ois.readObject();
				cervezas.add(cerveza);							
			} while ( cerveza != null);		
			return true;	
			
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage());
			e.printStackTrace();
			return false;
		}	
	}
	
	public ArrayList<Cerveza> getCervezas() {
		return cervezas;
	}
}
