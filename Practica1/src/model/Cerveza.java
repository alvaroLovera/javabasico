package model;

import java.io.Serializable;

public class Cerveza implements Serializable{

	private String nombre;
	private double graduacion;
	private int cantidad;
	private String tipo;
	private String[] ingredientes;
	private String descripcion;

	
	public Cerveza(String titulo, double precio, int cantidad, String formato, String[] ingredientes, String descripcion) {
		this.nombre = titulo;
		this.graduacion = precio;
		this.cantidad = cantidad;
		this.tipo = formato;
		this.ingredientes = ingredientes;
		this.descripcion = descripcion;
	}

	public Cerveza() {
		
	}

	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public double getGraduacion() {
		return graduacion;
	}



	public void setGraduacion(double graduacion) {
		this.graduacion = graduacion;
	}



	public int getCantidad() {
		return cantidad;
	}



	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}



	public String getTipo() {
		return tipo;
	}



	public void setTipo(String tipo) {
		this.tipo = tipo;
	}



	public String[] getIngredientes() {
		return ingredientes;
	}



	public void setIngredientes(String[] ingredientes) {
		this.ingredientes = ingredientes;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public boolean formatoIsValid(String formato) {
		boolean exito = false;
		if(formato.equalsIgnoreCase("DVD")||
				formato.equalsIgnoreCase("BLUERAY")||
				formato.equalsIgnoreCase("DESCARGA-DIGITAL")) {
			exito = true;
		}	
		return exito;
	}

}
