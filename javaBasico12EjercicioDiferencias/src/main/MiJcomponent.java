package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

public class MiJcomponent extends JComponent implements MouseListener {

	BufferedImage original = null;
	BufferedImage diferencias = null;
	boolean diferencias1 = false;
	boolean diferencias2 = false;
	boolean diferencias3 = false;
	int diferenciasX1, diferenciasY1, diferenciasX2, diferenciasY2, diferenciasX3, diferenciasY3;
	String pathFotoOriginal, pathFotoDiferencias;

	public MiJcomponent() {

		File archivoOriginal = new File(pathFotoOriginal);
		File archivoDiferencias = new File(pathFotoDiferencias);
		// File es una clase que representa archivos en Java
		// lo vamos a usar para cargar el archivo de imagen
		try {
			original = ImageIO.read(archivoOriginal);
			diferencias = ImageIO.read(archivoDiferencias);
		} catch (IOException e) {
			System.out.println("No tengo el archivo" + e.getMessage());
			// e.printStackTrace();
		}
		this.diferenciasX1 = diferenciasX1;
		this.diferenciasY1 = diferenciasY1;
		this.diferenciasX2 = diferenciasX2;
		this.diferenciasY2 = diferenciasY2;
		this.diferenciasX3 = diferenciasX3;
		this.diferenciasY3 = diferenciasY3;
		// Vamos a decir que MiJComponen atienda sus propios clicks de raton
		this.addMouseListener(this);
	}
	/*
	 * public MiJcomponent() { File archivoOriginal = new File("original.png"); File
	 * archivoDiferencias = new File("diferencias.png"); //File es una clase que
	 * representa archivos en Java //lo vamos a usar para cargar el archivo de
	 * imagen try { original = ImageIO.read(archivoOriginal); diferencias =
	 * ImageIO.read(archivoDiferencias); } catch (IOException e) {
	 * System.out.println("No tengo el archivo"+e.getMessage()); //
	 * e.printStackTrace(); }
	 * 
	 * //Vamos a decir que MiJComponen atienda sus propios clicks de raton
	 * this.addMouseListener(this); }
	 */

	@Override
	public void paint(Graphics g) {
		g.drawImage(original, 0, 0, this.getWidth() / 2, 400, this);
		g.drawImage(diferencias, 400, 0, this.getWidth() / 2, 400, this);
	}// end paint()

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	/*
	 * @Override public void mouseReleased(MouseEvent e) {
	 * System.out.println("click en: "+ e.getX()+" "+e.getY()); if(diferencias1 &&
	 * diferencias3 && diferencias3) { JOptionPane.showMessageDialog(this,
	 * "Felicidades adivinaste todas las diferencias"); } else if(e.getX()>=
	 * diferenciasX1 && e.getX() <= diferenciasX2 && e.getY() >= 155 && e.getY() <=
	 * 165) { diferencias1 = true; JOptionPane.showMessageDialog(this,
	 * "Felicidades adivinaste la diferencia 1"); } else if(e.getX()>= 557 &&
	 * e.getX() <= 586 && e.getY() >= 316 && e.getY() <= 347) { diferencias2 = true;
	 * JOptionPane.showMessageDialog(this,
	 * "Felicidades adivinaste la diferencia 2"); }else if(e.getX()>= 618 &&
	 * e.getX() <= 643 && e.getY() >= 37 && e.getY() <= 47) { diferencias3 = true;
	 * JOptionPane.showMessageDialog(this,
	 * "Felicidades adivinaste la diferencia 3"); }else {
	 * 
	 * JOptionPane.showMessageDialog(this, "Casi, sigue intentandolo"); }
	 * 
	 * }
	 */

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("click en: " + e.getX() + " " + e.getY());
		if (diferencias1 && diferencias3 && diferencias3) {
			JOptionPane.showMessageDialog(this, "Felicidades adivinaste todas las diferencias");
		} else if (e.getX() >= 559 && e.getX() <= 603 && e.getY() >= 155 && e.getY() <= 165) {
			diferencias1 = true;
			JOptionPane.showMessageDialog(this, "Felicidades adivinaste la diferencia 1");
		} else if (e.getX() >= 557 && e.getX() <= 586 && e.getY() >= 316 && e.getY() <= 347) {
			diferencias2 = true;
			JOptionPane.showMessageDialog(this, "Felicidades adivinaste la diferencia 2");
		} else if (e.getX() >= 618 && e.getX() <= 643 && e.getY() >= 37 && e.getY() <= 47) {
			diferencias3 = true;
			JOptionPane.showMessageDialog(this, "Felicidades adivinaste la diferencia 3");
		} else {
			JOptionPane.showMessageDialog(this, "Casi, sigue intentandolo");
		}

	}

}
