package main;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {

		JFrame ventana = new JFrame("Haz clic en las diferencias de la derecha");
		MiJcomponent miJcomponent = new MiJcomponent();
		ventana.setSize(800, 400);
		ventana.setLocation(100, 100);
		ventana.setContentPane(miJcomponent);
		ventana.setDefaultCloseOperation(3);
		ventana.setVisible(true);

		if (miJcomponent.diferencias1 && miJcomponent.diferencias2 && miJcomponent.diferencias3) {
			MiJcomponent miJcomponent2 = new MiJcomponent();
			ventana.setContentPane(miJcomponent2);
		}
	}

}
