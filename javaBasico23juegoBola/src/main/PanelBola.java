package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

public class PanelBola extends JComponent implements Runnable, KeyListener {
	int acceleracion = 2;
	Thread hilo = null;
	boolean hiloActivo = false;
	Bola bola = new Bola(50, 50, 50, 50, Color.ORANGE);
	ElemetoVisual meta = new ElemetoVisual(700, 300, 100, 200, Color.GREEN);
	ElemetoVisual obstaculo01 = new ElemetoVisual(123, 112, 60, 100, Color.RED);
	ElemetoVisual obstaculo02 = new ElemetoVisual(223, 210, 40, 20, Color.RED);
	ElemetoVisual obstaculo03 = new ElemetoVisual(430, 300, 70, 80, Color.RED);

	Random rnd = new Random();
	Date seed = new Date();

	public PanelBola() {

		rnd.setSeed(seed.getTime());
		hilo = new Thread(this);
		hilo.start();
		addKeyListener(this);
	}

	@Override
	public void paint(Graphics g) {
		bola.pintar(g);
		meta.pintar(g);

		obstaculo01.pintar(g);
		obstaculo02.pintar(g);
		obstaculo03.pintar(g);
	}

	@Override
	public void run() {
		hiloActivo = true;
		while (hiloActivo) {
			bola.mover();

			if (bola.detectarColision(meta)) {
				JOptionPane.showMessageDialog(this, "Feliciadades has ganado");
				hiloActivo = false;
				bola.setX(50);
				bola.setY(50);

				acceleracion = acceleracion + bola.getAcceleracion();
				bola.setAcceleracion(acceleracion);
				hiloActivo = true;
			}
			if (bola.detectarColision(obstaculo01) || bola.detectarColision(obstaculo02)
					|| bola.detectarColision(obstaculo03)) {

				// int randomNumX01 = ThreadLocalRandom.current().nextInt(500,700);
				perder();
			}
			if (bola.getX() > 800 || bola.getY() > 600 ||
					bola.getX() < 0 || bola.getY() < 0) {
				perder();
			}

			repaint();
			try {

				Thread.sleep(12);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void perder() {
		JOptionPane.showMessageDialog(this, "Has Perdido");
		hiloActivo = false;
		bola.setX(50);
		bola.setY(50);
		acceleracion = 0;
		bola.setAcceleracion(acceleracion);
		hiloActivo = true;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// System.out.println("Has pulsado la tecla de c�digo: "+ e.getKeyCode());
		// Derecha 39
		// Abajo 40
		// arriba 38
		// izquierda 37
		switch (e.getKeyCode()) {
		case 39:
			bola.setDireccion(Direccion.DERECHA);
			break;
		case 40:
			bola.setDireccion(Direccion.ABAJO);
			break;
		case 38:
			bola.setDireccion(Direccion.ARRIBA);
			break;
		case 37:
			bola.setDireccion(Direccion.IZQUIERDA);
			break;
		default:
			break;
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isFocusTraversable() {
		return true;
	}

}
