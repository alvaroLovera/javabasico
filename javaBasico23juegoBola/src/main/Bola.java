package main;

import java.awt.Color;
import java.awt.Graphics;

public class Bola extends ElemetoVisual{

	private Direccion direccion = Direccion.DERECHA;
	private int acceleracion = 0;
	
	
	public Bola(int x, int y , int ancho, int largo , Color color) {
		super(x, y, ancho, largo, color);
		
		
	}
	
	public boolean detectarColision(ElemetoVisual ev) {
		
		if(getX() >= ev.getX() && getX() <= (ev.getX() + ev.getAncho())
			&&
			getY() >= ev.getY() && getY() <= (ev.getY() + ev.getAlto())){
				//se ha chocado
				return true;
			}else {
				return false;
			}
	}
	
	public void mover() {
		switch (direccion) {
		case DERECHA:
			int x = getX();
			x++;
			x = x + acceleracion;
			setX(x);
			break;
		case ABAJO:
			int y = getY();
			y++;
			y = y + acceleracion;
			setY(y);
			break;
		case ARRIBA:
			y = getY();
			y--;
			y = y - acceleracion;
			setY(y);
			break;
		case IZQUIERDA:
			x = getX();
			x--;
			x = x - acceleracion;
			setX(x);
			break;
		default:
			break;
		}
	}
	
	@Override
	public void pintar(Graphics g) {
		
		g.setColor(getColor());
		g.fillOval(getX(), getY(), getAncho(), getAlto());
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public int getAcceleracion() {
		return acceleracion;
	}

	public void setAcceleracion(int acceleracion) {
		this.acceleracion = acceleracion;
	}

	
}
