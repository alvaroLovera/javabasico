package main;

import java.awt.Color;
import java.awt.Graphics;

public class ElemetoVisual {
	
	private int x, y, ancho, alto;
	private Color color;
	
	
	public ElemetoVisual() {
	}


	public ElemetoVisual(int x, int y, int ancho, int alto, Color color) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.color = color;
	}

	public void pintar(Graphics g) {
		
		g.setColor(color);
		g.fillRect(x, y, ancho, alto);	
	}
	
	
	public int getX() {
		return x;
	}


	public void setX(int x) {
		this.x = x;
	}


	public int getY() {
		return y;
	}


	public void setY(int y) {
		this.y = y;
	}


	public int getAncho() {
		return ancho;
	}


	public void setAncho(int ancho) {
		this.ancho = ancho;
	}


	public int getAlto() {
		return alto;
	}


	public void setAlto(int alto) {
		this.alto = alto;
	}


	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}
		
}
