package datos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class LibrosDAO extends MasterDAO{

	private static final String NOMBRE_ARCHIVO = "datos.dat";
	private static ArrayList<Libro> libros = new ArrayList<Libro>();

	public void guardarLibro(Libro l) {
		conectar();
		
		//libros.add(l);
	}

	public ArrayList<Libro> obtenerLibros() {
		return libros;
	}

	public void borrarLibro(Libro l) {

	}

	public boolean guardarArchivoLibros() {
		try {
			File file = new File(LibrosDAO.NOMBRE_ARCHIVO);
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(libros);

			oos.close();
			return true;
		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
			return false;
		}
	}

	/*
	 * A la hora de atender exepciones pudeo rodear con try catch
	 * o agraga throwa al metodo para quien lo llame rodee la
	 * llamada con un try-catch
	 */
	public boolean cargarArchivoLibros() {
		File file = new File(NOMBRE_ARCHIVO);
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);			
			libros = (ArrayList<Libro>) ois.readObject();			
			ois.close();			
			return true;
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
}
