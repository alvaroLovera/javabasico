package paneles;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import datos.LibrosDAO;
import datos.Libro;

public class PanelListarLibros extends JPanel{
	
	private JTextArea listado = new JTextArea(40,40); 
	
	public PanelListarLibros() {
		add(listado);
	}
	
	public void refrescarListado() {
		
		LibrosDAO librosDAO = new LibrosDAO();
		ArrayList<Libro> libros = librosDAO.obtenerLibros();
		listado.setText("");
		String texto = "";
		for (Libro l : libros) {
			texto += "titulo " + l.getTitulo();
			texto += "paginas " + l.getPaginas();
			texto += "precio " + l.getPrecio();
			texto += "\n";
		}
		listado.setText(texto);
	}
}
