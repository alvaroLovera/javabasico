package Ventanas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import datos.LibrosDAO;
import paneles.PanelInsertarLibro;
import paneles.PanelListarLibros;

public class VentanaPrincipal extends JFrame implements ActionListener {
	
	PanelInsertarLibro pInsertarLibro = new PanelInsertarLibro();
	PanelListarLibros pListaLibros = new PanelListarLibros();
	// Barra de Menu
	JMenuBar barraMenu = new JMenuBar();
	JMenu menuInsertar = new JMenu("Insertar");
	JMenu menuListar = new JMenu("Listar");
	JMenu menuArchivor = new JMenu("Archivo");
	
	JMenuItem menuItemInsertarLibro = new JMenuItem("Libro");
	JMenuItem menuItemListarLibros = new JMenuItem("Libros");
	JMenuItem menuItemArchivoCargarLibros = new JMenuItem("Cargar Libros");
	JMenuItem menuItemArchivoGuardarLibros = new JMenuItem("Guardar Libros");

	public VentanaPrincipal() {
		
		// Montar barra menu
		// Agrgar menu a barra de menus
		// Preparar submenus
		menuInsertar.add(menuItemInsertarLibro);
		menuListar.add(menuItemListarLibros);
		menuArchivor.add(menuItemArchivoCargarLibros);
		menuArchivor.add(menuItemArchivoGuardarLibros);
		
		barraMenu.add(menuArchivor);
		barraMenu.add(menuInsertar);
		barraMenu.add(menuListar);	
		setJMenuBar(barraMenu);
		
		// Quien atiende a los submenus
		menuItemInsertarLibro.addActionListener(this);
		menuItemListarLibros.addActionListener(this);
		menuItemArchivoCargarLibros.addActionListener(this);
		menuItemArchivoGuardarLibros.addActionListener(this);
		
		// Preparar Ventana
		setSize(600, 600);
		setLocation(100, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setContentPane(pInsertarLibro);
		setVisible(true);
	}



	@Override
	public void actionPerformed(ActionEvent ae) {
		System.out.println("Se ejecuta actionPerformed");
		// ae es el ActionEvent que contiene información sobre quein disparo el evento actual
		JMenuItem pulsado = (JMenuItem) ae.getSource();
		
		// (JMenuItem transforma lo que obtenemos loe que obtenemos por
		// ae.getSource() a tipo JMenuItem, si no lo hicieramos da error
		// ya que ae.getSource el elemento pulsado en forma de objeto generico (tipo Object)
		if(pulsado == menuItemInsertarLibro) {
			System.out.println("Mostrar pamel insertar libro");
			setContentPane(pInsertarLibro);
			
		}else if(pulsado == menuItemListarLibros) {
			System.out.println("mostrar panel lsitar libros");
			setContentPane(pListaLibros);
			pListaLibros.refrescarListado();
		}else if(pulsado==menuItemArchivoCargarLibros) {
			System.out.println("Cargar archivo de libros");
			LibrosDAO librosDAO = new LibrosDAO();
			boolean resultado = librosDAO.guardarArchivoLibros();
			if(resultado) {
				JOptionPane.showMessageDialog(this, "Libros guardados" );
			}else {
				JOptionPane.showMessageDialog(this, "No se pudo guardar" );
			}
		}else if(pulsado == menuItemArchivoGuardarLibros) {
			System.out.println("guardar libros en un archivo");
			LibrosDAO librosDAO = new LibrosDAO();
			librosDAO.cargarArchivoLibros();
		}
		
		// Fuerza refresco de la ventana
		SwingUtilities.updateComponentTreeUI(this);
	}
}
