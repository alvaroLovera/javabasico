package main;

public class Runnable1 implements Runnable {

	boolean activo = false;
	
	
	public void desactivarHilo() {
		activo = false;
	}

	@Override
	public void run() {

		activo = true;
		while (activo) {

			System.out.println("Hola desde Hilo 2");

			try {
				
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}// End while

	}

}
