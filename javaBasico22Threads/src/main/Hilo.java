package main;
//Las clases del paquete java.lang no necesitan de import
//por qur ya estan incluidas al considerarse clases b�sicas
public class Hilo extends Thread{

	//El c�digo que desee lanzar en un thread
	//Sera el c��digo contennido en el metodo ru
	boolean activo = false;

	@Override
	public void run() {
		activo = true;
		
		while (activo) {
			
			System.out.println("Hola desde hilo 1 !");
			try {
				sleep(1000);
			} catch (InterruptedException e) {

				System.out.println("hilo Interrumpido");
				e.printStackTrace();
			}
		}
		
	}// end run
	
	
	
}//end class
