package main;

public class Main {

	public static void main(String[] args) {
		
		// Ejemplo de uso de Treads 
		// En java hay dos opciones para crear un treahf
		// 1- Heredar de la clase threa
		//2-  Iplementar el interfaz Runnable
		
		Hilo hilo1 = new Hilo();
		hilo1.start();//Lanza el c�digo del m�todo run en un nuevo thread
		Runnable1 r1 = new Runnable1();
		Thread tr1 = new Thread(r1);
		tr1.start();
		// 
		//nUCA USAR .STOP() ni tampoco el .run()
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		r1.desactivarHilo();
	}

}
