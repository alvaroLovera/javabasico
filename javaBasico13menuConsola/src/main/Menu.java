package main;

import java.util.Scanner;

public class Menu {

	public void mostrarMenuPrincipal() {
		System.out.println("Introduce una Opci�n");
		System.out.println("1- Registra un libro");
		System.out.println("2- Listar libro");
		System.out.println("0- Salir");
	}
	
	public Libro mostrarMenuInsertarLibro(Scanner sc) {
		System.out.println("Introduce los datos del libro");
		Libro nuevo = new Libro();
		System.out.println("Introduce titulo");
		String titulo = sc.nextLine();
		System.out.println("Introduce paginas");
		String paginas = sc.nextLine();
		System.out.println("Introduce precio");
		String precio = sc.nextLine();
		
		nuevo.titulo = titulo;
		nuevo.numeropaginas = Integer.parseInt(paginas);
		nuevo.precio = Double.parseDouble(precio);
		
		return nuevo;
	}
}
