package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainMenu {

	public static void main(String[] args) {
		
		//teemos q crear un menu de opciones
		//y todo  lo q sea de menu, se crea en una clase
		
		int opcionInsertada = 2;
		
		//creo un scanner q ira leyendo un teclado
		
		Scanner miScanner = new Scanner(System.in);
		Menu menu = new Menu();
		
		//se crea una lista para guardar los libros
		//que no me deja crear tipo  bew list
		//tiene q ser ArrayList
		
		List<Libro> libros = new ArrayList<>();
		
		
		//el bucle while es identico al if con la excepcion 
		//de q cuando el while termina se vueve a comprobar su condicion 
		//y si se cumple, se ejecuta de nuevo.
		
        while(opcionInsertada != -1) {
	        System.out.println("se ejecuta el while");
	        menu.mostrarMenuPrincipal();
	        String insertado = miScanner.nextLine();
	        System.out.println("Has insertado:" + insertado);
	        
	        int insertadoInt = Integer.parseInt(insertado);
	        switch (insertadoInt) {
	        case 1: 
	        	System.out.println("has introducido 1");
	        	Libro nuevo = menu.mostrarMenuInsertarLibro(miScanner);
	        	//nuevo es lo q mostrarmenuinsertarlibro ha devuelto el meni.java
	        	System.out.println("insertado:" + nuevo.titulo);
	        	libros.add(nuevo);
	            break;
	        case 2:
	        	System.out.println("has intriducido 2");
	        	menu.listarLibros(libros);
	        	break;
	        case 3:
	        	System.out.println("has intriducido 3");
	        	menu.borrarLibros(miScanner,libros);
	     
	        	break;
	        case 4:
	        	System.out.println("has intriducido 3");
	        	menu.editarLibro(miScanner,libros);
	        	break;
	        case 0:
	        	System.out.println("has elegido salir");
	        	//cambiamso la opcion insertada a -1 para 
	        	//q deverdad salga, por la condicion de while
	        	//q pusimos al principio
	        	opcionInsertada = -1;
	        	break;
	        default:
	        	break;
	        }//end switch
		}//end while
	}
}
