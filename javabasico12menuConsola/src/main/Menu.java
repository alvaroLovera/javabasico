package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

public class Menu {
	//creamos el metodo de mostrar
	//el menu principal c las diferentes opciones
	public void mostrarMenuPrincipal() {
		System.out.println("introduce una opcion:");
		System.out.println("1-registar libro");
		System.out.println("2-listar libros");
		System.out.println("3-Borrar libros");
		System.out.println("4-Editar libros");
		System.out.println("0-salir");
	}
	//resto de metodos
	public Libro mostrarMenuInsertarLibro(Scanner miScanner) {
		System.out.println("Introduce los datos del libro");
		Libro nuevo = new Libro();
		System.out.println("titulo");
		String tituloInsertado = miScanner.nextLine();
		System.out.println("paginas");
		String paginas = miScanner.nextLine();
		System.out.println("precio:");
		String precio = miScanner.nextLine();
		
		nuevo.titulo = tituloInsertado;
		nuevo.numPaginas = Integer.parseInt(paginas);
		nuevo.precio = Double.parseDouble(precio);
		//al hacer un return el metodo finaliza
		//y debuelve a quien lo llam� la informacion indicada,
		//en este caso el nuevo libro
		//y en void hay q poner el tipo de dato del return.
		//si el metodo hace un return tenemos q cambiar
		//la palabra void por el tipo de dato de dicho elemento
		return nuevo;
		
	}
	public void listarLibros(List<Libro> libros) {
		System.out.println("Listado de libros:");
		for (Libro elemento : libros) {
			System.out.println("Titulo: "+elemento.titulo);
		}
	}
	public void borrarLibros(Scanner miScanner, List<Libro> libros) {
		listarLibros(libros);
		System.out.println("Inserta el t�tulo del libro a borrar");
		String tituloBorrar = miScanner.nextLine();
		ArrayList<Libro> librosAborrar =
				new ArrayList<Libro>();
		for (Libro libro : libros) {
			if(libro.titulo.equals(tituloBorrar)) {
				librosAborrar.add(libro);
			}
		}
		//Una vez conseguidos los titulos del libro a borrar 
		//pues los borro
		libros.removeAll(librosAborrar);
		System.out.println("Libros Borrados");
		
	}
	public void editarLibro(Scanner miScanner, List<Libro> libros) {
		listarLibros(libros);
		System.out.println("Introduce el t�tulo del libro a editar");
		String tituloAeditar = miScanner.nextLine();
		Libro libroAeditar = null;
		for (Libro libro : libros) {
			if(libro.titulo.equals(tituloAeditar)){
				libroAeditar = libro;
			}
		}
		System.out.println("Introduce el titulo Nuevo");
		String tituloNuevo = miScanner.nextLine();
		System.out.println("Introduce el NumeroPag");
		String pagoNuevo = miScanner.nextLine();
		System.out.println("Introduce el titulo Nuevo");
		String precioNuevo = miScanner.nextLine();
		
		libroAeditar.titulo = tituloNuevo;
		libroAeditar.precio = Double.parseDouble(precioNuevo);
		libroAeditar.numPaginas = Integer.parseInt(pagoNuevo);
		
		
	}
	
}
