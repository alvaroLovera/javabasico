package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		//teemos q crear un menu de opciones
		//y todo  lo q sea de menu, se crea en una clase
		int opcionInsertada = -1;
		//creo un scanner q ira leyendo un teclado
		Scanner miScanner = new Scanner(System.in);
		
		System.out.println("Introduce tu nombre");
		// la siguiente linea espera a q introduzca
		//algo por teclado y cuando lo mete en la variable nombre
		String nombre = miScanner.nextLine();
		System.out.println("nombre:" + nombre);
	}

}
