package ventanas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class VentanaPrincipal extends JFrame implements ActionListener{


	JLabel jLabel = new JLabel();
	JButton jBotonConvertirAdolares = new JButton();
	JPanel jPanel = new JPanel();
	JTextField jTFeuros = new JTextField(6);
	
	public VentanaPrincipal() {
		
		prepararYmostrarVentana();
	}
	
	
	public void prepararYmostrarVentana() {
		setSize(300, 250);
		setLocation(500, 250);
		setDefaultCloseOperation(3);
		setTitle("Conversor de Lereles");
		
		
		jLabel.setText("Convierte Euros a Otras Monedas");
		jBotonConvertirAdolares.setText("Dolares");
		jBotonConvertirAdolares.addActionListener(this);
		
		jPanel.add(jLabel);
		jPanel.add(jTFeuros);
		jPanel.add(jBotonConvertirAdolares);
		
		
		add(jPanel);
		
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		String introducido = jTFeuros.getText();
		if(isValidMoneda(introducido)) {
			if(containsPuntoOnly(introducido)) {
				float euros = Float.parseFloat(introducido);
				float dolares = euros * 1.1f;	
				JOptionPane jOpDolares = new JOptionPane();
				jOpDolares.showMessageDialog(this, " "+introducido+"Euros son: "+dolares+" Dolares");
			}else if(containsComaOnly(introducido)) {
				String unidadesYdecimales[] = introducido.split(",");
				String paraFloat = unidadesYdecimales[0] +"."+ unidadesYdecimales[1];
				float euros = Float.parseFloat(paraFloat);
				float dolares = euros * 1.1f;	
				JOptionPane jOpDolares = new JOptionPane();
				jOpDolares.showMessageDialog(this, " "+introducido+"Euros son: "+dolares+" Dolares");
			}
		}else {
			JOptionPane jOpDolares = new JOptionPane();
			jOpDolares.showMessageDialog(this,"Error de Formato de Monedas");
		}
		
	}
	
	public boolean isValidMoneda(String moneda) {
		moneda = moneda.trim();	
		Pattern patron = Pattern.compile("[0-9]*.?,?[0-9]*");
		Matcher encaja = patron.matcher(moneda);
		System.out.println(moneda+" "+encaja.matches());
		
		return encaja.matches();
	}
	
	public boolean containsPuntoOnly(String moneda) {
		moneda = moneda.trim();	
		Pattern patron = Pattern.compile(".{1}");
		Matcher encaja = patron.matcher(moneda);		
		return encaja.find();
	}
	public boolean containsComaOnly(String moneda) {
		moneda = moneda.trim();	
		Pattern patron = Pattern.compile(",{1}");
		Matcher encaja = patron.matcher(moneda);		
		return encaja.find();
	}
/*
 * if(moneda.contains(",")) {	
			String unidadesYdecimales[] = moneda.split(",");
			
		}	
 */

}
