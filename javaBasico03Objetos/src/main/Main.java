package main;

public class Main {

	public static void main(String[] args) {
		
		//vamos a fabricar varias bolas
		Bola bola1 = new Bola();
		//ahora mismo es como si todo los que hubiese en Bola 
		//estuvies en bola1
		Bola bola2 = new Bola();
		Bola bola3 = new Bola();
		bola1.mover();
		bola2.mover();
		bola3.mover();

	}

}
