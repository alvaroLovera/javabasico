package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;



public class Main {

	public static void main(String[] args) {
		// Ejemplo de uso de jdbc
		// 1� Cargar el driver
		//una vez importado el .jar de mysql
		// cargamos su siguiente clase
		
		//Class.forName permite que la clase que le indiquemos "" 
		//se cargue por memoria y pueda lanzar un 
		//codigo suyo de configuracion
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("La clase no existe: "+e.getMessage());
			e.printStackTrace();
		}
		//2� Establecer una conexion con la base de datos
		Connection con = null;
		
		
		try {
			String url = "jdbc:mysql://localhost:3306/bd_libros?useUnicode=true&useJDBCCompliantTimezoneShift="
					+ "true&useLegacyDatetimeCode=false&serverTimezone=UTC";
					//"jdbc:mysql://localhost:3306/bd_libros";
			String usuario = "root";
			String pass = "admin";
			
			con = DriverManager.getConnection(url,usuario,pass);
		} catch (SQLException e) {
			System.out.println("Error de conexi�n: "+e.getMessage());
		}
		
		//3� Una vez obtenida la conexion realizamos consultas
		String sql = "INSERT INTO tabla_libros(titulo,paginas,precio) values('prueba2',123,45.5)";
		try {
			Statement st = con.createStatement();
			st.execute(sql);
			System.out.println("Prueba OK");
		} catch (SQLException e) {
			System.out.println("ERROR: "+e.getMessage());
		}
		try {
			con.close();
		} catch (SQLException e) {
			System.out.println("ERRO de cierre de conexion: "+e.getMessage());
			e.printStackTrace();
		}
	}

}
