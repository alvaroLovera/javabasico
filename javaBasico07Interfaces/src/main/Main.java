package main;

import entrenador.EntrenadorPersonal;
import jugadores.Delantero;
import jugadores.Portero;

public class Main {

	public static void main(String[] args) {

		Delantero juan = new Delantero();
		Delantero ana = new Delantero();
		Portero sonia = new Portero();
		//la ventaja de usar muchas interfaces 
		// es que tengo garantizados los m�todos correr y parar
		// en delantero , Portero y toda aquella clase
		//que quiera implementar el interfaz Jugador
		
		//Podemos tratar tantos los delanteros como los porteros 
		//como si fueran del mismo tipo de jugador
		
		EntrenadorPersonal maria = new EntrenadorPersonal();
		maria.mandarCorrer(ana);
		maria.mandarCorrer(sonia);
	}

}
