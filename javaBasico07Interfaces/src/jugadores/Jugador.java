package jugadores;

public interface Jugador {
	//desde aqui marcamos que m�todos deben tener las 
	//clases que cumplan con esta interfaz
	
	public void correr();
	
	public void parar();
}
