package paneles;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.print.attribute.standard.JobName;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import datos.LibrosDAO;
import datos.Libro;

public class PanelListarLibros extends JPanel{
	
	private JTable tabla;
	private MiTableModel miTableModel;
	private String[] cabecerasTabla = {"T�tulo","P�ginas","Precio"};
	private ArrayList<Libro> libros;
	private JButton botonBorrar;
	private JButton botonEditar;
	private JScrollPane barraDesplaz;
	//UNA CLASE PUEDE ESTAR DENTRO DE OTRA ES ALGO ADECUADO
	//cUANDO	UNA CLAse depende integramente de otraa y no
	// tiene sentido usarlas de forma separada
	//La siguiente clase va a decir como v a a ser la tabla 
	//que vamos a mosttrar en el panel mostrar libros
	class MiTableModel extends AbstractTableModel {

		
		
		@Override
		public String getColumnName(int column) {
			
			return cabecerasTabla[column];
		}

		@Override
		public int getColumnCount() {
			return cabecerasTabla.length;
		}

		@Override
		public int getRowCount() {
			return libros.size();
		}

		@Override
		public Object getValueAt(int fila, int columna) {
			/*
			 * Java llama a este metodo autamaticamente
			 * para saber qur mostrar en que fila y columna
			 * la primera fila sera el primer libro
			 * y la segunda el segundo
			 * y asi sucesivamente
			 * por eso la siguiente linea de codigo 
			 * nos valr para obtener el libro de la fila indicada
			 */
			Libro libro = libros.get(fila);
			String info = "";
			switch (columna) {
			case 0: info = libro.getTitulo();
			break;
			case 1: info = Integer.toString(libro.getPaginas());
			break;
			case 2: info = libro.getPrecio() + " euros";
			break;
			default:
				break;
			}
			return info;
		}
		
	}//END MiTableModel
	
	
	public PanelListarLibros() {
		/*ya tengo el JTable
		 * ya tengo creada la clase que dice como e 
		 * va mostrar la table con los botones de edita y borrar
		 */
		
		tabla = new JTable(miTableModel);
		//aunque mi table model no sea nada la tabla ya lo tiene asignado y lo que 
		//meta en mi table sera lo que muestre la tabla
		botonBorrar = new JButton("BORRAR");
		botonEditar = new JButton("EDITAR");
		
		tabla.setPreferredScrollableViewportSize(new Dimension(300,70));
		tabla.setFillsViewportHeight(true);
		barraDesplaz = new JScrollPane(tabla);
		
		add(barraDesplaz);
		add(botonBorrar);
		add(botonEditar);
		
		
	}
	
	public void refrescarListado() {
		LibrosDAO librosDAO = new LibrosDAO();
		libros = librosDAO.obtenerLibros();
		miTableModel = new MiTableModel();
		tabla.setModel(miTableModel);
		// Una vez sacados lo libros que hay y una vez sacados los libros que hay
		//le digo a la tabla que se refreque
		miTableModel.fireTableDataChanged();
	}
	
	
}
