package main;

public class Pelicula {
	
	String titulo = "";
	double precio = 0.0;
	int duracion = 0;
	String formato = "";
	
	public Pelicula() {
	}
	
	public Pelicula(String titulo, double precio, int duracion, String formato) {
		super();
		this.titulo = titulo;
		this.precio = precio;
		this.duracion = duracion;
		this.formato = formato;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	
	public boolean formatoIsValid(String formato) {
		boolean exito = false;
		if(formato.equalsIgnoreCase("DVD")||
				formato.equalsIgnoreCase("BLUERAY")||
				formato.equalsIgnoreCase("DESCARGA-DIGITAL")) {
			exito = true;
		}	
		return exito;
	}

}
