package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		int opcionInsertada = 2;
		Scanner miScanner = new Scanner(System.in);
		Menu menu = new Menu();
		List<Pelicula> peliculas = new ArrayList<>();
		
	    while(opcionInsertada != -1){
	        System.out.println("se ejecuta el while");
	        menu.mostrarMenuPrincipal();
	        String insertado = miScanner.nextLine();
	        System.out.println("Has insertado:" + insertado);
	        
	        int insertadoInt = Integer.parseInt(insertado);
	        switch (insertadoInt) {
		        case 1: 
		        	System.out.println("has introducido 1");
		        	Pelicula nuevo = menu.mostrarMenuInsertarPelicula(miScanner);
		        	System.out.println("insertado:" + nuevo.getTitulo());
		        	if(nuevo.formatoIsValid(nuevo.getFormato())) {
		        		peliculas.add(nuevo);
		        	}else {
		        		System.out.println("Formato no valido ");
		        	}
		            break;
		        case 2:
		        	System.out.println("has intriducido 2");
		        	menu.listarPeliculas(peliculas);
		        	break;
		        case 3:
		        	System.out.println("has intriducido 3");
		        	menu.listarPeliculas(peliculas);
		        	System.out.println("Introduca el titulo de la pelicula que desea borrar");
		    		String tituloInsertado = miScanner.nextLine();
		    		//for(Pelicula p : peliculas) {
		    		for(int i = 0;i<peliculas.size();i++) {
		    			if(peliculas.get(i).getTitulo().equalsIgnoreCase(tituloInsertado)) {
		    				if(peliculas.remove(peliculas.get(i)))System.out.println("Borrada con exito");
		    				//break;
		    			}
		    		}
		    	//	}
		        	break;
		        case 4:
		        	System.out.println("has intriducido 4");
		        	menu.listarPeliculas(peliculas);
		        	System.out.println("Introduca el titulo de la pelicula que desea editar");
		    		String tituloInsertadoEdit = miScanner.nextLine();
		    		boolean encontrada = false;
		    		Pelicula peliEditar = null;
		    		for(Pelicula p : peliculas) {
		    			if(p.getTitulo().equalsIgnoreCase(tituloInsertadoEdit)) {	    				
		    					System.out.println("Pelicula seleccionanda con con exito");
		    					encontrada = true;
		    					peliEditar = p;
		    			}
		    		}
		    		if(!encontrada) { 
		    			System.out.println("La pelicula no la tenemos");
		    			}else{
		    				Pelicula peliEditada = menu.menuEditarPelicula(peliEditar,miScanner);
		    				
		    			}
		    		
		        	break;
		        case 0:
		        	System.out.println("has elegido salir");        
		        	opcionInsertada = -1;
		        	break;
		        default:
		        	break;
		        }//end switch
			}//end while
	}
	
}

