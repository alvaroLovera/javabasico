package main;

import java.util.List;
import java.util.Scanner;

public class Menu {

	public void mostrarMenuPrincipal() {
		System.out.println("introduce una opcion:");
		System.out.println("1-registar pelicula");
		System.out.println("2-listar peliculas");
		System.out.println("3-Borrar pelicula");
		System.out.println("4-Editar pelicula");
		System.out.println("0-salir");
	}
	
	public Pelicula mostrarMenuInsertarPelicula(Scanner miScanner) {
		System.out.println("Introduce los datos de la pelicula");
		Pelicula nuevo = new Pelicula();
		System.out.println("titulo");
		String tituloInsertado = miScanner.nextLine();
		nuevo.setTitulo(tituloInsertado);
		System.out.println("precio");
		String precio = miScanner.nextLine();
		nuevo.setPrecio(Double.parseDouble(precio));
		System.out.println("duracion:");
		String duracion = miScanner.nextLine();
		nuevo.setDuracion(Integer.parseInt(duracion));		
		System.out.println("formato:");
		String formato = miScanner.nextLine();
		nuevo.setFormato(formato);
		return nuevo;
	}
	
	public Pelicula menuEditarPelicula(Pelicula p, Scanner miScanner) {
		System.out.println("Introduce los nuevos datos de la pelicula");
		Pelicula nuevo = p;
		System.out.println("titulo");
		String tituloInsertado = miScanner.nextLine();
		nuevo.setTitulo(tituloInsertado);
		System.out.println("precio");
		String precio = miScanner.nextLine();
		nuevo.setPrecio(Double.parseDouble(precio));
		System.out.println("duracion:");
		String duracion = miScanner.nextLine();
		nuevo.setDuracion(Integer.parseInt(duracion));		
		System.out.println("formato:");
		String formato = miScanner.nextLine();
		nuevo.setFormato(formato);
		
		return nuevo;
	}
	
	public void listarPeliculas(List<Pelicula> peliculas) {
		System.out.println("Listado de Pelicula:");
		for (Pelicula elemento : peliculas) {
			System.out.println("Titulo: "+elemento.getTitulo()+" Precio: "+
		elemento.getPrecio()+"Formato: "+elemento.getFormato());
		}
	}
	
}
