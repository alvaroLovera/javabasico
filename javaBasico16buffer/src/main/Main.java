package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

	public static void main(String[] args) {
		
		File file = new File("texto.txt");
		if(file.exists()) {
			System.out.println("El archivo ya existe");
			try {
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				String frase = "";
				do {
					
					frase = br.readLine();
					System.out.println(frase);
					
				} while (frase != null);
				
			} catch (Exception e) {
				System.out.println("No tengo permisos para leer el fichero");
			}
			
		}else {
			System.out.println("El archivo no existe");

			try {
//				file.createNewFile();
				FileWriter fw = new FileWriter(file);
				PrintWriter pw = new PrintWriter(fw);
				pw.write("frase 1 sdffffffffffffffffffffffffffffffffffffffffffffffffffffffffsdfffffffff");
				pw.write("frase 2");
				pw.close();
			} catch (IOException e) {
				System.out.println("Error de acceso al archivo");
			}
			

	}

	}
}
