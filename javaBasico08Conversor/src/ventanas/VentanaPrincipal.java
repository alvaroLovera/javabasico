package ventanas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//Para que esta ventana pueda antender
//botones suyos  o de  cualquier otro 
//sitio se debe implementar el  interfaz ActionListener
public class VentanaPrincipal extends JFrame implements ActionListener {

	JButton boton = new JButton();
	JLabel etiqueta = new JLabel();
	JTextField campoCentigrados = new JTextField(6);
	JPanel panel = new JPanel();
	
	
	//Constructor
	//su codigo se ejecutar� cada vez que se haga un new VentanaPrincipal
	//es decir cada vez que se cree un objeto
	//eS UN METODO MUY �TIL PARA PREPARAR COSAS
	public VentanaPrincipal() {
		System.out.println("Se ejecuta el constructor");
		prepararYmostrarVentana();
	}
	
	public void prepararYmostrarVentana() {
		//Como esta clase hereda de JFrame puedo 
		//llamar directamente a metodos de JFrame
		setSize(400, 150);
		setLocation(500, 230);
		setTitle("Introduce unos grados Centigrados");
		setDefaultCloseOperation(3);
		//preparamos los componentes
		etiqueta.setText("Centigrados: ");
		boton.setText("Convertir");
		//Le digo al boton que le va a atendeter la propia
		//Ventana principal.L a vantanaPrincipal 
		//puede atender botoners porque tiene el implements ActionListener
		boton.addActionListener(this);
		
		//Preparar el panel 
		panel.add(etiqueta);
		panel.add(campoCentigrados);
		panel.add(boton);
		
		add(panel);
		
		setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Se ejecita el actionPerfomed");
		//Asi declaro un String llamado introducido que va a vales lo que 
		//Haya introducido el usuario en campoCentigrados
		String introducido = campoCentigrados.getText();
		System.out.println("Introducido: "+ introducido);
		//HAY que sumar 273 a lo introducido para transformarlo a grados Kelvin
		//No podemos operar con elementos tipo String para ello 
		//hay que transformar el String a tipo float con parte fraccionaria
		float centigrados = Float.parseFloat(introducido);
		float kelvin = centigrados + 273;
		
		System.out.println("kelvin: "+kelvin);
		JDialog jDialogKelvin = new JDialog(this,"En Kelvin es "+kelvin);
		jDialogKelvin.setVisible(true);
		
	}

}
