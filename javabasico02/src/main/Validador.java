package main;

public class Validador {

	//A la hora de ddefinir un m�todo hay dos importantes
	//declararlo como static: 
	//Ese elelmento lo podr� invocar el m�todo directamente
	//Tambi�n se puede no declararlo como static
	// para invocar el m�todo habra que crear un objeto 
	//de la clase donde esst� el m�todo
	
	//Ejemplo uso de static
	public static void validarEmail() {
		System.out.println("Validando email");	
	}
	
}
