package main;

public class Main {

	public static void main(String[] args) {

		int numero = 0;//int solo para enteros
		numero = 10;
	//	int grande= 9999999999999; int tiene un tope 
		long muyGrande = 999999999999999999l;// un long soporta el doble que un int
		//para especificar un long se pone l o L al final
		short pequeno = 9999;//Para numeros la mitad de grandes que un int
		
		/*
		 * Para numeros con parte fraccionaria
		 */
		float grados = 1222222.3224f;//f o F al final siempre
		// el float es similar al int
		double precio = 10000000.666;//d o D al final opcional
		
		//para caracteres
		char letra = 'a';//entre comas simples
		
		//String para frases
		String frases = "hola corazones";
		
		//objetos
		Usuario ana = new Usuario();
		//Los tipos basico de datos, se usan directamente
		//y no tienen nada dentro poruqe no son objetos
		
		//Los string no son tiopos basicos de dato , son objetos
		//de una clase llamadfa String
		
		//tantofrases como ana sion objetos de dos clases 
		//disc
		//tintas, La unica diferencia es que para e� casp cpmcero de un objeto String no hace
		//falta usar el operador new.
		
		
		byte numeroMuyPeque�o = 127;
		boolean interruptor = false;
		//Los unicos valores validos para un boolean son true o false
		
		//wrappers son clases que hacen la funcion de objeto para los tipos basicos de datos
		
		Integer intObjeto = new Integer(6);
		Double DOUBLEoJETO = new Double(8.8);
		Float floatObjeto = new Float(9.9F);
		Boolean booleaObg = new Boolean(true);
		Character charObj = new Character('a');
		
		System.out.println("N�mero vale: "+numero);

	}

}
