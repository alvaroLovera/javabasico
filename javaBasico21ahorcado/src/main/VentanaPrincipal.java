package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class VentanaPrincipal extends JFrame implements ActionListener{

	
	private String fraseAadivinar = "Cosas de casa";
	private String fraseAmostrar = "";
	private JLabel labelFraseAmostrar = new JLabel();
	private JTextField entrada = new JTextField(10);
	private JButton botonAdivinar = new JButton("Adivinar");
	private int puntos = 100;
	
	private JLabel labelDesvelar = new JLabel("Desvelar Letra");
	private JLabel labelPuntuacions = new JLabel(String.valueOf(puntos));
	private JTextField entradaDesvelar = new JTextField(3);
	private JButton desvelarLetra = new JButton("DESVELAR");
	
	public VentanaPrincipal() {
		
		char[] fraseArray = fraseAadivinar.toCharArray();
		
		for (char c : fraseArray) {
			if (c == ' ') {
				fraseAmostrar += " ";
			}else {
				fraseAmostrar += "_";
			}
		}
		labelFraseAmostrar.setText(fraseAmostrar);
		//agregar controles
		JPanel panel = new JPanel();
		panel.add(labelFraseAmostrar);
		panel.add(entrada);
		panel.add(botonAdivinar);
		panel.add(labelDesvelar);
		panel.add(entradaDesvelar);
		panel.add(desvelarLetra);
		
		// Atender botones
		botonAdivinar.addActionListener(this);
		desvelarLetra.addActionListener(this);
		
		add(panel);
		
		configurarVentanaPrincipal();
	}

	public void configurarVentanaPrincipal() {
		setSize(400,100);
		setLocation(450, 150);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Adivina la frase");
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Object pulsado = e.getSource();
		if(pulsado == botonAdivinar) {
			
			//COMOPROBAR QUE SE HA INSERTADO LA FRASE A ADIVINAR
			if(fraseAadivinar.equalsIgnoreCase(entrada.getText())) {
				
			}
			
			
		}else if(pulsado == desvelarLetra) {
			
			
			
			
			String introducido = entradaDesvelar.getText();
			char letraIntroducida = introducido.charAt(0);
			char [] fraseAadivinarCharArray = fraseAadivinar.toCharArray();
			char [] fraseAmostrarCharArray = fraseAmostrar.toCharArray();
			//Voy a recorrer letra a letra la frase a adivinar para saber donde est� la
			//letra que ha introduido el usuario
			for (int i = 0; i < fraseAadivinarCharArray.length; i++) {				
				if(fraseAadivinarCharArray[i] == letraIntroducida) {				
					fraseAmostrarCharArray[i] = letraIntroducida;					
				}		
			}
			fraseAmostrar = new String(fraseAmostrarCharArray);
			labelFraseAmostrar.setText(fraseAmostrar);
			entradaDesvelar.setText("");
			
		}
		
		
	}

}
