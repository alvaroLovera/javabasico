package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		
		File file = new File("texto.txt");
		if(file.exists()) {
			System.out.println("El archivo ya existe");
			try {
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				int letra = -1;
				do {
					letra = fr.read();
					System.out.println((char)letra);
					
				}while( letra != -1 );
				//Si lee -1 es que ha llegado al final del archivo
			} catch (Exception e) {
				System.out.println("No tengo permisos para leer el fichero");
			}
			
		}else {
			System.out.println("El archivo no existe");
			//file.mkdirs();
			
//			file.mkdirs();
			try {
				file.createNewFile();
				FileWriter fw = new FileWriter(file);
				fw.write("Besis de fresi \n");
				fw.write("Hasta luegui \n");
				fw.close();
			} catch (IOException e) {
				System.out.println("Error de acceso al archivo");
			}
			
			
//			try {
//				file.createNewFile();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		}

	}

}
