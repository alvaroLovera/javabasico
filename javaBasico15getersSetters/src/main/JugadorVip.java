package main;

public class JugadorVip extends Jugador{
	
	private int diasPremium = 0;
	
	public JugadorVip(String nombre, int puntuacion) {
		super(nombre,puntuacion);
		
	}

	@Override
	public String toString() {
		return super.toString() + "JugadorVip [diasPremium=" + diasPremium + "]";
	}

	
}
