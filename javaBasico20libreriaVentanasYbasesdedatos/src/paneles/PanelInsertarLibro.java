package paneles;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import datos.Libro;
import datos.LibrosDAO;

public class PanelInsertarLibro extends JPanel implements ActionListener{

	JLabel titulo = new JLabel("Insertar Libro");
	GridBagConstraints gbc = new GridBagConstraints();
	GridBagLayout gbl = new GridBagLayout();
	JLabel labelTitulo = new JLabel("T�tulo: ");
	JLabel labelPaginas = new JLabel("P�ginas: ");
	JLabel labelPrecio = new JLabel("Precio: ");
	
	JTextField jTfTitulo = new JTextField(10);
	JTextField jTfpaginas = new JTextField(10);
	JTextField jTfPrecio = new JTextField(10);
	
	JButton botonRegistrarLibro = new JButton("REGISTRAR");
	
	
	
	public PanelInsertarLibro() {
		setLayout(gbl);
		// Primera fila
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(labelTitulo,gbc);
		gbc.gridx = 1;
		add(jTfTitulo,gbc);
		// segunda fila
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(labelPaginas,gbc);
		// terera fila
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(jTfpaginas,gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(labelPrecio,gbc);
		gbc.gridx = 1;
		gbc.gridy = 2;
		add(jTfPrecio,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		add(botonRegistrarLibro,gbc);
		
		botonRegistrarLibro.addActionListener(this);
	}



	@Override
	public void actionPerformed(ActionEvent ae) {
		
		String titulo = jTfTitulo.getText();
		String paginas = jTfpaginas.getText();
		String precio = jTfPrecio.getText();
		//validdaciones
		if(titulo.trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "T�tulo vacio");
			return; // Al poner return el actionPerformed no sigue y termina
		}
		int paginasInt = 0;
		try {
			paginasInt = Integer.parseInt(paginas);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Entrada de p�ginas no v�lida");
			return;
		}
		double precioDouble = 0.0;
		try {
			precioDouble = Double.parseDouble(precio);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Entrada de precio no v�lida");
			return;
		}
		
		Libro nuevo = new Libro(titulo,paginasInt,precioDouble);
		//nuevo.setTitulo(titulo);
		//nuevo.mostrarDatos();
		LibrosDAO librosDAO = new LibrosDAO();
		librosDAO.guardarLibro(nuevo);
		
		JOptionPane.showMessageDialog(this, "Libro guardado Correctamente");
		
	}// end actionPerformed
	
}
