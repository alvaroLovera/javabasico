package datos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class LibrosDAO extends MasterDAO {

	private static final String NOMBRE_ARCHIVO = "datos.dat";
	private static ArrayList<Libro> libros = new ArrayList<Libro>();

	public void guardarLibro(Libro l) {
		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_INSERCION_LIBRO);
			ps.setString(1, l.getTitulo());
			ps.setInt(2, l.getPaginas());
			ps.setDouble(3, l.getPrecio());

			ps.execute();
		} catch (SQLException e) {
			System.out.println("Error Prepared Stament: " + e.getMessage());
			e.printStackTrace();
		}
		libros.add(l);
		desconectar();
	}

	public ArrayList<Libro> obtenerLibros() {
		// COMO ahora usamos bases de datos en vez de uar el satatic list definido
		// arriba,
		// vamos a crear una lista que tendr� los lobros de la BBDD
		ArrayList<Libro> librosBaseDatos = new ArrayList<Libro>();
		conectar();
		String sql = ConstantesSQL.SQL_SELECT_LIBROS;
		Statement st;
		try {
			st = conexion.createStatement();
			ResultSet rs = st.executeQuery(sql);
			// A Partir de esta linea se que he obtenido una rs correctamente
			// rs tiene un metodo que se llama rs.next(); 
			// que nos situa al rs en la siguiente fila que aun no hemos proccesado 

			//si next nos devuelve false, es que no queedan filas
			// por procesar del resultado	
			while (rs.next()) {
				Libro libro = new Libro();
				libro.setId(rs.getInt("id"));
				libro.setTitulo(rs.getString("titulo"));			
				libro.setPaginas(rs.getInt("paginas"));
				libro.setPrecio(rs.getDouble("precio"));				
				librosBaseDatos.add(libro);			
			}
		} catch (SQLException e) {
			System.out.println("ERROR STAMENT: " + e.getMessage());
			e.printStackTrace();
		}
		desconectar();
	
		return librosBaseDatos;
	}

	public void borrarLibro(Libro l) {
		
	}

	public boolean guardarArchivoLibros() {
		try {
			File file = new File(LibrosDAO.NOMBRE_ARCHIVO);
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(libros);

			oos.close();
			return true;
		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
			return false;
		}
	}

	/*
	 * A la hora de atender exepciones pudeo rodear con try catch o agraga throwa al
	 * metodo para quien lo llame rodee la llamada con un try-catch
	 */
	public boolean cargarArchivoLibros() {
		File file = new File(NOMBRE_ARCHIVO);
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			libros = (ArrayList<Libro>) ois.readObject();
			ois.close();
			return true;
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}

}
