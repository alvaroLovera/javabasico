package datos;

import java.io.Serializable;

//Si quiero guardar objetos de esta clase, la clase
//debe implementar el interfa seriealizable
public class Libro implements Serializable{
	
	private int id;
	private String titulo;
	private int paginas;
	private double precio;
	
	// PAra permitir crear un objeto libro tanto
	// sin darle nada como fanfdole un t�tulo, paginas y precio 
	// creamos uno vacio
	public Libro() {
		
	}
	
	// LO siguiente es un costructor que exihge que para tener un objeto de libro hay que dar un objeto de libro
	// hay que dar un titulo unas paginas y un precio
	public Libro(String titulo, int paginas, double precio) {
		this.titulo = titulo;
		this.paginas = paginas;
		this.precio = precio;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getPaginas() {
		return paginas;
	}

	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public void mostrarDatos() {
		System.out.println("Datos del Libro");
		System.out.println(this.titulo);
		System.out.println(this.paginas);
		System.out.println(this.precio);
	}
	
	
}
