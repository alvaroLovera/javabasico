package datos;

public class ConstantesSQL {
	public static final String SQL_INSERCION_LIBRO = "insert into tabla_libros "
			+ "(titulo,paginas,precio) value(?,?,?)";
	
	public static final String SQL_SELECT_LIBROS = "SELECT * FROM tabla_libros ";
	
	public static final String SQL_DELETE_LIBRO = "DELETE FROM tabla_libros "
			+ "WHERE id = ?";
	

}
