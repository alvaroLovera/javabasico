package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Cerveza implements Serializable{

	private int id;
	private String marca;
	private double precio;
	private int cantidad;
	private String ingredientes;
	private String opinion;
	private String descripcion;
	
	
	public Cerveza(String marca, double precio, int cantidad, String ingredientes, String opinion,
			String descripcion) {
		this.marca = marca;
		this.precio = precio;
		this.cantidad = cantidad;
		this.ingredientes = ingredientes;
		this.opinion = opinion;
		this.descripcion = descripcion;
	}


	public Cerveza(int id, String marca, double precio, int cantidad, String ingredientes, String opinion,
			String descripcion) {
		this.id = id;
		this.marca = marca;
		this.precio = precio;
		this.cantidad = cantidad;
		this.ingredientes = ingredientes;
		this.opinion = opinion;
		this.descripcion = descripcion;
	}

	public Cerveza() {
		
	}

	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	

	public String getIngredientes() {
		return ingredientes;
	}


	public void setIngredientes(String ingredientes) {
		this.ingredientes = ingredientes;
	}


	public String getOpinion() {
		return opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
