package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class CervezaDAO extends MasterDAO {

	private static final String NOMBRE_ARCHIVO = "cervezas.dat";
	private ArrayList<Cerveza> cervezas = new ArrayList<Cerveza>();
	private static final CervezaDAO instancia = new CervezaDAO();

	private CervezaDAO() {

	}

	public static CervezaDAO getInstancia() {
		return instancia;
	}

	public void guardarCerveza(Cerveza c) {

		conectar();
		try {
			PreparedStatement ps = conexion.prepareStatement(ConstantesSQL.SQL_INSERCION_CERVEZA);
			ps.setString(1, c.getMarca());
			ps.setDouble(2, c.getPrecio());
			ps.setInt(3, c.getCantidad());
			ps.setString(4, c.getIngredientes());
			ps.setString(5, c.getOpinion());
			ps.setString(6, c.getDescripcion());

			ps.execute();
		} catch (SQLException e) {
			System.out.println("Error Prepared Stament: " + e.getMessage());
			e.printStackTrace();
		}
		// cervezas.add(c);
		desconectar();
	}

	public int numeroCervezas() {
		return cervezas.size();
	}

	public ArrayList<Cerveza> getCervezas() {

		List<Cerveza> cervezaBaseDatos = new ArrayList<Cerveza>();
		String sql = ConstantesSQL.SQL_SELECT_CERVEZAS;
		conectar();
		Statement st;

		try {
			st = conexion.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {

				Cerveza cerveza = new Cerveza();
				cerveza.setId(rs.getInt("id"));
				cerveza.setMarca(rs.getString(2));
				cerveza.setPrecio(rs.getDouble(3));
				cerveza.setCantidad(rs.getInt(4));
				cerveza.setIngredientes(rs.getString(5));
				cerveza.setOpinion(rs.getString(6));
				cerveza.setDescripcion(rs.getString(7));

				cervezaBaseDatos.add(cerveza);
			}

		} catch (SQLException e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}

		desconectar();
		return (ArrayList) cervezaBaseDatos;
	}

	public void borrarCerveza(int id) {

		conectar();

		String sql = ConstantesSQL.SQL_DELETE_CERVEZA;
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setInt(1, id);
			ps.execute();

		} catch (SQLException e) {
			System.out.println("ERROR Prepared Statement: " + e.getMessage());
			e.printStackTrace();
		}

		desconectar();

	}

	public void editarCerveza(Cerveza cervezaAeditar) {
		
		conectar();
			
		String sql = ConstantesSQL.SQL_UPDATE_CERVEZA;		
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setString(1, cervezaAeditar.getMarca());
			ps.setDouble(2, cervezaAeditar.getPrecio());
			ps.setInt(3, cervezaAeditar.getCantidad());
			ps.setString(4, cervezaAeditar.getIngredientes());
			ps.setString(5, cervezaAeditar.getOpinion());
			ps.setString(6, cervezaAeditar.getDescripcion());
			ps.setInt(7, cervezaAeditar.getId());
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("ERROR Prepared Statement: " + e.getMessage());
			e.printStackTrace();
		}
		
		desconectar();
	}

	public boolean guardarCervezasArchivo(File file) {

		try {
		//	File file = new File(NOMBRE_ARCHIVO);
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			// for (Cerveza c : cervezas) {
			
			oos.writeObject(cervezas = getCervezas());
			// }
			// oos.writeObject(cervezas);
			oos.close();
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			return false;
		}
		return true;
	}
	
	public void borrarTodasLasCervezas() {
		conectar();
		
		String sql = ConstantesSQL.SQL_BORRAR_TODAS_CERVEZAS;
		try {
			Statement st = conexion.createStatement();
			st.execute(sql);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		desconectar();
		
	}
	

	public boolean cargarFichero(File file) {
	//	File file = new File(NOMBRE_ARCHIVO);
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);

			
			ArrayList<Cerveza>	cervezasArchivo = 
					(ArrayList<Cerveza>) ois.readObject();
				
			borrarTodasLasCervezas();
			
			for (Cerveza cerveza : cervezasArchivo) {
				
				guardarCerveza(cerveza);			
			}
			
			ois.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR: " + e.getMessage());
			
			return false;
		}
		return true;
	}
	
	public Cerveza buscarCerveza(int idCerveza) {
		
		conectar();
		Cerveza cerveza = new Cerveza();
		String sql = ConstantesSQL.SQL_OBTENER_CERVEZA_POR_ID;
		try {
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.setInt(1, idCerveza);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				cerveza.setId(rs.getInt("id"));
				cerveza.setMarca(rs.getString("marca"));
				cerveza.setPrecio(rs.getDouble("precio"));
				cerveza.setCantidad(rs.getInt("cantidad"));
				cerveza.setIngredientes(rs.getString("ingredientes"));
				cerveza.setOpinion(rs.getString("opinion"));
				cerveza.setDescripcion(rs.getString("descripcion"));			
			}
			
		} catch (SQLException e) {
			System.out.println("ERROR PREPA.STAMENT: " + e.getMessage());
			e.printStackTrace();
			return null;
		}		
		desconectar();	
		return cerveza;
	}

}
