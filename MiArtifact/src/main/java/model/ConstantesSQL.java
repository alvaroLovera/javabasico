package model;

public class ConstantesSQL {
	public static final String SQL_INSERCION_CERVEZA = "insert into cervezas "
			+ "(marca,precio,cantidad,ingredientes,opinion,descripcion) value(?,?,?,?,?,?)";

	public static final String SQL_SELECT_CERVEZAS = "SELECT * FROM cervezas ";

	public static final String SQL_DELETE_CERVEZA = "DELETE FROM cervezas " + "WHERE id = ?";
	
	public static final String SQL_UPDATE_CERVEZA = "UPDATE cervezas SET marca = ?, "
			+ "precio = ?, cantidad = ?, ingredientes = ?, opinion = ?, descripcion = ?  WHERE id = ?";

	public static final String SQL_OBTENER_CERVEZA_POR_ID = "SELECT * FROM cervezas where id = ?";
	
	public static final String SQL_BORRAR_TODAS_CERVEZAS = "DELETE FROM cervezas " + "WHERE id > 0 ";
}
