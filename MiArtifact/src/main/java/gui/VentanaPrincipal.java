package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import model.Cerveza;
import model.CervezaDAO;

public class VentanaPrincipal extends JFrame implements ActionListener {

	PanelInsertarCerveza pInsertarCerveza = new PanelInsertarCerveza();
	PanelListarCervezas pListaCervezas = new PanelListarCervezas(this);
	PanelEditarCerveza pEditarCerveza = new PanelEditarCerveza();
	// Barra de Menu
	// JMenu file = new JMenu("File");
	// menubar.add(file);
	// JMenuItem newgame = new JMenuItem("New");
	// file.add(newgame);

	JMenuBar barraMenu = new JMenuBar();
	JMenu menuInsertar = new JMenu("Insertar");
	JMenu menuListar = new JMenu("Listar");
	JMenu menuArchivo = new JMenu("Archivo");
	JMenuItem menuItemInsertarCerveza = new JMenuItem("Cerveza");
	JMenuItem menuItemListaCervezas = new JMenuItem("Cervezas");
	JMenuItem menuItemGuardarArchivo = new JMenuItem("Guardar en Archivo");
	JMenuItem menuItemCargarArchivo = new JMenuItem("Cargar datos desde Archivo");
	// JTable tabla = new JTable();

	public VentanaPrincipal() {
		// mostrarVentana();
		menuItemInsertarCerveza.setIcon(new ImageIcon("cerveza.png"));

		menuInsertar.add(menuItemInsertarCerveza);
		menuListar.add(menuItemListaCervezas);
		menuArchivo.add(menuItemGuardarArchivo);
		menuArchivo.add(menuItemCargarArchivo);

		barraMenu.add(menuInsertar);
		barraMenu.add(menuListar);
		barraMenu.add(menuArchivo);
		setJMenuBar(barraMenu);

		menuItemInsertarCerveza.addActionListener(this);
		menuItemListaCervezas.addActionListener(this);
		menuItemGuardarArchivo.addActionListener(this);
		menuItemCargarArchivo.addActionListener(this);

		setSize(900, 600);
		setLocation(300, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setContentPane(pInsertarCerveza);

		this.setIconImage(new ImageIcon(getClass().getResource("/gui/iconoCerveza.jpg")).getImage());

		setVisible(true);
	}

	public void mostrarVentana() {
		setSize(700, 500);
		setLocation(300, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// JLabel Jltitulo = new JLabel();
		// JTextField JtFtitulo = new JTextField();
		// JLabel Jlprecio = new JLabel();
		// JTextField JtFprecio = new JTextField();
//		JLabel Jlduracion = new JLabel();
//		JTextField JtFduracion = new JTextField();	
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {

		// CervezaDAO cervezas = new CervezaDAO();
		Object pulsado = ae.getSource();

		System.out.println("Action Commadn: VN " + ae.getActionCommand());
		// ae.getActionCommand() devuelve el nombre del boton
		if (ae.getActionCommand().equals("EDITAR")) {

			int idAeditar = pListaCervezas.obtenerIdSeleccionado();
			System.out.println(idAeditar);
			setContentPane(pEditarCerveza);
			pEditarCerveza.mostrarCervezaAeditar(idAeditar);
		}else if (ae.getActionCommand().equals("BORRAR")) {

			int idAeditar = pListaCervezas.obtenerIdSeleccionado();
			System.out.println(idAeditar);
			setContentPane(pEditarCerveza);
			pEditarCerveza.mostrarCervezaAeditar(idAeditar);
		}else if (pulsado == menuItemInsertarCerveza) {
			setContentPane(pInsertarCerveza);
			System.out.println("Has pulsado Insertar");
		}else if (pulsado == menuItemListaCervezas) {
			// pListaCervezas.refrescarJlistPeliculas();
			setContentPane(pListaCervezas);
			// TmCervezas tm = new TmCervezas();
			// pListaCervezas.tabla.setModel(tm);
			pListaCervezas.refrescarCervezas();

			System.out.println("Has pulsado Listar");
		}else if (pulsado == menuItemGuardarArchivo) {
			// Voy a presentarle al usuario un selector de archivos
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {

				File file = chooser.getSelectedFile();
				System.out.println("El archivo elegido es " + file.getName());

				CervezaDAO.getInstancia().guardarCervezasArchivo(file);

				JOptionPane.showMessageDialog(this, "Archivo guardado con �xito");

			}

			/*
			 * if (CervezaDAO.getInstancia().guardarCervezasArchivo()) {
			 * System.out.println("Guardado con �xito"); } else {
			 * System.out.println("Error de guardado"); }
			 */
		}else if (pulsado == menuItemCargarArchivo) {
			// Vamos a pedir al usuario que elija el archivo
			JFileChooser chooser = new JFileChooser();
			int returVal = chooser.showOpenDialog(this);

			if (returVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				CervezaDAO.getInstancia().cargarFichero(file);
				// PONER AVISO DE FALLO O CARGA EXITOSA !!!!!!!!!!
			}

		}
		SwingUtilities.updateComponentTreeUI(this);
	}

	public PanelEditarCerveza getpEditarCerveza() {
		return pEditarCerveza;
	}

}
