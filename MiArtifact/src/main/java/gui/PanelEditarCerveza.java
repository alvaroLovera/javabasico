package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import model.Cerveza;
import model.CervezaDAO;
import model.Constantes;

public class PanelEditarCerveza extends JPanel implements ActionListener {

	private int idSeleccionado;
	// private ArrayList<Cerveza> cervezas;
	private Cerveza cervezaAeditar = null;
	private GridBagConstraints gbc = new GridBagConstraints();
	private GridBagLayout gbl = new GridBagLayout();

	private JLabel tituloPanel = new JLabel("Editar una Cerveza");
	private JLabel labelMarca = new JLabel("Marca: ");
	private JLabel labelPrecio = new JLabel("Precio: ");
	private JLabel labelCantidad = new JLabel("Cantida: ");
	private JLabel labelIgredientes = new JLabel("Ingredientes: ");
	private JLabel labelopinion = new JLabel("Opini�n: ");
	private JLabel labelDescripcion = new JLabel("Descipci�n: ");

	private JTextField tFmarca = new JTextField(10);
	private JTextField tFopinion = new JTextField(10);
	private JComboBox<String> comboCantidad = new JComboBox<String>(Constantes.CANTIDAD);

	private JTextArea tAdescripcion = new JTextArea(5, 10);

	// Precio
	private JRadioButton rBotonUno = new JRadioButton("1 $");
	private JRadioButton rBotonCinco = new JRadioButton("5 $");
	private JRadioButton rBotonDiez = new JRadioButton("10 $");
	private JRadioButton rBotonQuince = new JRadioButton("15 $");

	// Ingredientes
	JCheckBox checkLupulo = new JCheckBox("L�pulo");
	JCheckBox checkTrigo = new JCheckBox("Trigo");
	JCheckBox checkMalta = new JCheckBox("Malta");
	JCheckBox checkCebada = new JCheckBox("Cebada");

	JButton botonEditarCerveza = new JButton("Guardar Cambios");

	public PanelEditarCerveza() {
		setSize(600, 600);

		// tFmarca.setText(aEditar.getMarca());
		// tFopinion.setText(aEditar.getOpinion());

		ButtonGroup bg = new ButtonGroup();
		bg.add(rBotonUno);
		bg.add(rBotonCinco);
		bg.add(rBotonDiez);
		bg.add(rBotonQuince);

		setLayout(gbl);
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(tituloPanel, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		add(labelMarca, gbc);
		// segunda fila
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(tFmarca, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		add(labelPrecio, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		add(rBotonUno, gbc);

		gbc.gridx = 1;
		gbc.gridy = 3;
		add(rBotonCinco, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		add(rBotonDiez, gbc);

		gbc.gridx = 1;
		gbc.gridy = 5;
		add(rBotonQuince, gbc);

		// tercera fila
		gbc.gridx = 0;
		gbc.gridy = 7;
		add(labelCantidad, gbc);
		gbc.gridx = 1;
		gbc.gridy = 7;
		add(comboCantidad, gbc);
		// cuarta fila
		gbc.gridx = 0;
		gbc.gridy = 8;
		// gbc.gridwidth = 2;
		add(labelIgredientes, gbc);

		gbc.gridx = 0;
		gbc.gridy = 9;
		add(checkLupulo, gbc);

		gbc.gridx = 1;
		gbc.gridy = 9;
		add(checkTrigo, gbc);

		gbc.gridx = 0;
		gbc.gridy = 10;
		add(checkMalta, gbc);

		gbc.gridx = 1;
		gbc.gridy = 10;
		add(checkCebada, gbc);

		gbc.gridx = 0;
		gbc.gridy = 11;
		add(labelopinion, gbc);

		gbc.gridx = 1;
		gbc.gridy = 11;
		add(tFopinion, gbc);

		gbc.gridx = 0;
		gbc.gridy = 12;
		add(labelDescripcion, gbc);

		gbc.gridx = 1;
		gbc.gridy = 14;
		add(tAdescripcion, gbc);

		gbc.gridx = 0;
		gbc.gridy = 15;
		gbc.gridwidth = 2;
		add(botonEditarCerveza, gbc);

		botonEditarCerveza.addActionListener(this);

		setVisible(true);
		// ventanaPrincipal.getpListaCervezas().obtenerIdSeleccionado();
		// mostrarCervezaAeditar(ventanaPrincipal.getpListaCervezas().obtenerIdSeleccionado());
	}

	public void mostrarCervezaAeditar(int idCervezaAeditar) {

		// sacammos toda la informacion
		// permitimos que se pueda modificar y luego actualizamos esos datos
		// en la BBDD

		cervezaAeditar = CervezaDAO.getInstancia().buscarCerveza(idCervezaAeditar);
		if (!cervezaAeditar.equals(null)) {

			tFmarca.setText(cervezaAeditar.getMarca());
			tAdescripcion.setText(cervezaAeditar.getDescripcion());
			tFopinion.setText(cervezaAeditar.getOpinion());
			if (cervezaAeditar.getCantidad() == 1) {
				comboCantidad.setSelectedIndex(0);
			} else if (cervezaAeditar.getCantidad() == 6) {
				comboCantidad.setSelectedIndex(1);
			} else if (cervezaAeditar.getCantidad() == 12) {
				comboCantidad.setSelectedIndex(2);
			} else if (cervezaAeditar.getCantidad() == 20) {
				comboCantidad.setSelectedIndex(3);
			}

			if (cervezaAeditar.getPrecio() == 1.0) {
				rBotonUno.setSelected(true);
			} else if (cervezaAeditar.getPrecio() == 5.0) {
				rBotonCinco.setSelected(true);
			} else if (cervezaAeditar.getPrecio() == 10.0) {
				rBotonDiez.setSelected(true);
			} else if (cervezaAeditar.getPrecio() == 15.0) {
				rBotonQuince.setSelected(true);
			}

			/// faltan los check boxs del los inredientes !!!!!!!!!!!!!!

		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String marca = tFmarca.getText();
		Double precio = 0.0;
		String ingrediente = "";
		ArrayList<String> ingredientesCheckBox = new ArrayList<String>();
		String cantidadString = comboCantidad.getSelectedItem().toString();
		int cantidadInt = 0;
		String descripcion = tAdescripcion.getText().trim();
		String opinion = tFopinion.getText().trim();

		// COmo aqui solo atendemos a el boton gaurdar cambios no hacemos diferenciacion
		// HACER VALIDACIONES !!!!!!!!!!!!!!!!!!!
		
		if (marca.trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Rellene el campo Marca");
			return;
		}
		if (!rBotonUno.isSelected() && !rBotonCinco.isSelected() && !rBotonDiez.isSelected()
				&& !rBotonQuince.isSelected()) {
			JOptionPane.showMessageDialog(this, "Seleccione un precio");
			return;
		}

		if (rBotonUno.isSelected()) {
			precio = 1.0;
		} else if (rBotonCinco.isSelected()) {
			precio = 5.0;
		} else if (rBotonDiez.isSelected()) {
			precio = 10.0;
		} else if (rBotonQuince.isSelected()) {
			precio = 15.0;
		}

		if (!checkLupulo.isSelected() && !checkTrigo.isSelected() && !checkMalta.isSelected()
				&& !checkCebada.isSelected()) {
			JOptionPane.showMessageDialog(this, "Seleccione al menos un ingrediente");
			return;
		}

		if (checkLupulo.isSelected()) {
			ingrediente = ingrediente + Constantes.INGREDIENTES[1];
		}
		if (checkTrigo.isSelected()) {
			ingrediente = ingrediente + Constantes.INGREDIENTES[2];
		}
		if (checkMalta.isSelected()) {
			ingrediente = ingrediente + Constantes.INGREDIENTES[0];
		}
		if (checkCebada.isSelected()) {
			ingrediente = ingrediente + Constantes.INGREDIENTES[3];
		}

		if (opinion.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Rellene el campo Opini�n");
			return;
		}

		if (descripcion.isEmpty() && descripcion.length() < 2 && descripcion.length() > 100) {
			JOptionPane.showMessageDialog(this, "Rellene la descripcion");
			return;
		}

		if (cantidadString.equals(Constantes.CANTIDAD[0])) {
			cantidadInt = 1;
		} else if (cantidadString.equals(Constantes.CANTIDAD[1])) {
			cantidadInt = 6;
		} else if (cantidadString.equals(Constantes.CANTIDAD[2])) {
			cantidadInt = 12;
		} else if (cantidadString.equals(Constantes.CANTIDAD[3])) {
			cantidadInt = 20;
		}

		// Cerveza nueva = new Cerveza(marca, precio, cantidadInt,
		// ingredientesCheckBox,opinion, descripcion);
		// CervezaDAO cervezas = new CervezaDAO();
		cervezaAeditar.setMarca(marca);
		cervezaAeditar.setCantidad(cantidadInt);
		cervezaAeditar.setIngredientes(ingrediente);
		cervezaAeditar.setOpinion(opinion);
		cervezaAeditar.setDescripcion(descripcion);
		cervezaAeditar.setPrecio(precio);

		CervezaDAO.getInstancia().editarCerveza(cervezaAeditar);
		JOptionPane.showMessageDialog(this, "Editado correctamente");

		SwingUtilities.updateComponentTreeUI(this);
		//setVisible(false);
		

	}

	public int getIdSeleccionado() {
		return idSeleccionado;
	}

	public void setIdSeleccionado(int idSeleccionado) {
		this.idSeleccionado = idSeleccionado;
	}

}
